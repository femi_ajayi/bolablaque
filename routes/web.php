<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('home');



Route::get('/dashboard', [
    'uses' => 'UserController@getDashboard',
    'as' => 'dashboard',
    'middleware' => 'auth'
]);

Route::post('/login', [
    'uses' => 'UserController@postLogIn',
    'as' => 'login',
]);

Route::get('/logout', [
    'uses' => 'UserController@getLogout',
    'as' => 'UserAccount.logout',
]);

Route::group(['prefix' => 'UserAccount'], function () {

    //   Route::group(['middleware' => 'guest'], function () {

    Route::get('/profile/{id}', [
        'uses' => 'UserController@getProfileDetails',
        'as' => 'UserAccount.profile',
        'middleware' => 'auth'
    ]);


    Route::get('/index', [
        'uses' => 'UserController@viewAllUsers',
        'as' => 'UserAccount.index',
        'middleware' => 'auth'
    ]);

    Route::get('/create', [
        'uses' => 'UserController@getNewUser',
        'as' => 'UserAccount.create',
        'middleware' => 'auth'
    ]);

    Route::post('/create', [
        'uses' => 'UserController@postNewUser',
        'as' => 'UserAccount.create',
        'middleware' => 'auth'
    ]);

    Route::post('/updatePassword', [
        'uses' => 'UserController@UpdatePassword',
        'as' => 'UserAccount.updatePassword',
        'middleware' => 'auth'
    ]);

    Route::get('delete/{id}', [
        'uses' => 'UserController@DeleteUser',
        'as'   => 'UserAccount.delete',
        'middleware' => 'auth'
    ]);



    //   });

});


Route::group(['prefix' => 'category'], function(){
    Route::get('', [
        'uses' => 'CategoryController@viewCategoryIndex',
        'as'   => 'category.index',
        'middleware' => 'auth'
    ]);

    Route::post('categoryName', [
        'uses' => 'CategoryController@viewCategoryByName',
        'as'   => 'category.categoryName',
        'middleware' => 'auth'
    ]);

    Route::get('create', [
        'uses' => 'CategoryController@getCategoryCreate',
        'as'   => 'category.create',
        'middleware' => 'auth'
    ]);

    Route::post('create', [
        'uses' => 'CategoryController@postCategoryCreate',
        'as'   => 'category.create',
        'middleware' => 'auth'
    ]);

    Route::get('edit/{id}', [
        'uses' => 'CategoryController@getCategoryEdit',
        'as'   => 'category.edit',
        'middleware' => 'auth'
    ]);

    Route::get('delete/{id}', [
        'uses' => 'CategoryController@getCategoryDelete',
        'as'   => 'category.delete',
        'middleware' => 'auth'
    ]);

    Route::post('delete', [
        'uses' => 'CategoryController@postCategoryDelete',
        'as'   => 'category.remove',
        'middleware' => 'auth'
    ]);

    Route::post('edit', [
        'uses' => 'CategoryController@postCategoryUpdate',
        'as'   => 'category.update',
        'middleware' => 'auth'
    ]);

    Route::get('addCategoryNumber', [
        'uses' => 'CategoryController@addCategoryNumber',
        'as'   => 'category.addNumber'
    ]);



});

Route::group(['prefix' => 'brand'], function(){
    Route::get('', [
        'uses' => 'BrandController@viewBrandIndex',
        'as'   => 'brand.index',
        'middleware' => 'auth'
    ]);

    Route::post('brandName', [
        'uses' => 'BrandController@viewBrandByName',
        'as'   => 'brand.brandName',
        'middleware' => 'auth'
    ]);

    Route::get('create', [
        'uses' => 'BrandController@getBrandCreate',
        'as'   => 'brand.create',
        'middleware' => 'auth'
    ]);

    Route::post('create', [
        'uses' => 'BrandController@postBrandCreate',
        'as'   => 'brand.create'
    ]);

    Route::get('edit/{id}', [
        'uses' => 'BrandController@getBrandEdit',
        'as'   => 'brand.edit',
        'middleware' => 'auth'
    ]);

    Route::get('delete/{id}', [
        'uses' => 'BrandController@getBrandDelete',
        'as'   => 'brand.delete',
        'middleware' => 'auth'
    ]);

    Route::post('delete', [
        'uses' => 'BrandController@postBrandDelete',
        'as'   => 'brand.remove',
        'middleware' => 'auth'
    ]);

    Route::post('edit', [
        'uses' => 'BrandController@postBrandUpdate',
        'as'   => 'brand.update',
        'middleware' => 'auth'
    ]);

    Route::get('addBrandNumber', [
            'uses' => 'BrandController@addBrandNumber',
            'as'   => 'brand.addNumber'
    ]);


});

Route::group(['prefix' => 'customer'], function(){
    Route::get('', [
        'uses' => 'CustomerController@viewCustomerIndex',
        'as'   => 'customer.index',
        'middleware' => 'auth'
    ]);

    Route::post('customerName', [
        'uses' => 'CustomerController@viewCustomerByName',
        'as'   => 'customer.customerName',
        'middleware' => 'auth'
    ]);

    Route::get('create', [
        'uses' => 'CustomerController@getCustomerCreate',
        'as'   => 'customer.create',
        'middleware' => 'auth'
    ]);

    Route::post('create', [
        'uses' => 'CustomerController@postCustomerCreate',
        'as'   => 'customer.create'
    ]);

    Route::get('edit/{id}', [
        'uses' => 'CustomerController@getCustomerEdit',
        'as'   => 'customer.edit',
        'middleware' => 'auth'
    ]);

    Route::get('orderDetails/{id}', [
        'uses' => 'CustomerController@getOrderDetails',
        'as'   => 'customer.orderDetails',
        'middleware' => 'auth'
    ]);

    Route::get('view/{id}', [
        'uses' => 'CustomerController@ViewCustomerDetails',
        'as'   => 'customer.view',
        'middleware' => 'auth'
    ]);

    Route::get('delete/{id}', [
        'uses' => 'CustomerController@getCustomerDelete',
        'as'   => 'customer.delete',
        'middleware' => 'auth'
    ]);

    Route::post('delete', [
        'uses' => 'CustomerController@postCustomerDelete',
        'as'   => 'customer.remove',
        'middleware' => 'auth'
    ]);

    Route::post('edit', [
        'uses' => 'CustomerController@postCustomerUpdate',
        'as'   => 'customer.update',
        'middleware' => 'auth'
    ]);

    Route::post('/fetch', 'CustomerController@fetch')->name('customer.fetch');

    Route::post('/search', [
        'uses' => 'CustomerController@searchCustomerByName',
        'as'   => 'customer.search',
        'middleware' => 'auth'
    ]);

    Route::get('/birthdays', [
        'uses' => 'CustomerController@getCustomerBirthdays',
        'as'   => 'customer.birthdays',
        'middleware' => 'auth'
    ]);


});


Route::group(['prefix' => 'product'], function(){

    Route::get('', [
        'uses' => 'ProductController@viewProductIndex',
        'as'   => 'product.index',
        'middleware' => 'auth'
    ]);

    Route::post('productName', [
        'uses' => 'ProductController@viewProductByName',
        'as'   => 'product.productName',
        'middleware' => 'auth'
    ]);

    Route::get('create', [
        'uses' => 'ProductController@getProductCreate',
        'as'   => 'product.create',
        'middleware' => 'auth'
    ]);

    Route::post('create', [
        'uses' => 'ProductController@postProductCreate',
        'as'   => 'product.create',
        'middleware' => 'auth'
    ]);

    Route::get('delete/{id}', [
        'uses' => 'ProductController@getProductDelete',
        'as'   => 'product.delete',
        'middleware' => 'auth'
    ]);

    Route::post('delete', [
        'uses' => 'ProductController@postProductDelete',
        'as'   => 'product.remove',
        'middleware' => 'auth'
    ]);

    Route::get('edit/{id}', [
        'uses' => 'ProductController@getProductEdit',
        'as'   => 'product.edit',
        'middleware' => 'auth'
    ]);

    Route::post('edit', [
        'uses' => 'ProductController@postProductUpdate',
        'as'   => 'product.update',
        'middleware' => 'auth'
    ]);

    Route::post('/fetch', [
        'uses' => 'ProductController@fetch',
        'as'   => 'product.fetch'
    ]);



});


Route::group(['prefix' => 'orders', 'middleware' => 'auth'], function(){
/*Route::group(['prefix' => 'orders'], function(){*/

    Route::get('', [
        'uses' => 'ProductController@viewRecentOrders',
        'as'   => 'orders.index'
    ]);

    Route::get('create', [
        'uses' => 'ProductController@getOrdersCreate',
        'as'   => 'orders.create'
    ]);

    Route::get('createName', [
        'uses' => 'ProductController@getOrdersCreateName',
        'as'   => 'orders.createName'
    ]);

    Route::post('checkout', [
        'uses' => 'ProductController@postCheckoutPage',
        'as'   => 'orders.checkout'
    ]);

    Route::get('sales', [
        'uses' => 'ProductController@getRecentSales',
        'as'   => 'orders.sales'
    ]);

    Route::get('print/{id}', [
        'uses' => 'ProductController@getPrintPage',
        'as'   => 'orders.print'
    ]);

    Route::get('re-print/{id}', [
        'uses' => 'ProductController@getRePrintPage',
        'as'   => 'orders.re-print'
    ]);

    Route::get('mail_receipt/{id}', [
        'uses' => 'ProductController@getMailReceipt',
        'as'   => 'mailReceipt'
    ]);

    Route::post('mail_receipt', [
        'uses' => 'ProductController@postMailReceipt',
        'as'   => 'postMailReceipt'
    ]);

    Route::get('return/{id}', [
        'uses' => 'ProductController@getReturnPage',
        'as'   => 'orders.return'
    ]);

    Route::post('/create', 'ProductController@getAddToCart')->name('ordersCreate');

    Route::post('/createName', 'ProductController@getAddToCartNyName')->name('ordersCreateName');


    Route::post('/increase', 'ProductController@increaseCartOrders')->name('ordersIncrease');

    Route::post('/decrease', 'ProductController@decreaseCartOrders')->name('ordersDecrease');

    Route::post('/delete', 'ProductController@deleteCartOrders')->name('ordersDelete');

    Route::post('/save_cart', 'ProductController@finalCartPage')->name('ordersSave');

    Route::post('/customerSearch', 'CustomerController@getCustomerDetails')->name('customerSearch');

    Route::post('/customerSearchName', 'CustomerController@getCustomerNameDetails')->name('customerSearchName');


    Route::post('/postReturn', 'ProductController@postReturn')->name('postReturn');


});

Route::get('/sync', 'ProductController@getSyncData')->name('syncData');

Route::post('/syncPost', 'ProductController@postSyncData')->name('postSyncData');

Route::post('/syncReceive', 'ProductController@receiveSyncData')->name('receiveSyncData');

Route::post('/add-to-cart',[
    'uses' => 'ProductController@getAddToCart',
    'as'   => 'orders.addToCart'
]);



Route::group(['prefix' => 'reports', 'middleware' => 'auth'], function(){
    Route::get('', [
        'uses' => 'ProductController@salesReport',
        'as'   => 'reports.sales'
    ]);

    Route::post('/reportSearch', [
        'uses' => 'ProductController@getSalesDetails',
        'as'   => 'reportSearch'
    ]);


    Route::get('/salesPerson', [
        'uses' => 'ProductController@getReportBySalesPerson',
        'as'   => 'reports.salesPerson'
    ]);

    Route::post('/salesPersonDetails', [
        'uses' => 'ProductController@postReportBySalesPerson',
        'as'   => 'reports.salesPersonDetails'
    ]);




});



Route::get('product/sendProductOnline', [
    'uses' => 'ProductController@sendProductOnline',
    'as'   => 'product.sendProductOnline'
]);

Route::get('product/receiveProduct', [
    'uses' => 'ProductController@receiveProduct',
    'as'   => 'product.receiveProduct'
]);

Route::get('brand/sendBrandOnline', [
    'uses' => 'BrandController@sendBrandOnline',
    'as'   => 'brand.sendBrandOnline'
]);

Route::get('brand/receiveBrand', [
    'uses' => 'BrandController@receiveBrand',
    'as'   => 'brand.receiveBrand'
]);

Route::get('category/sendCategoryOnline', [
    'uses' => 'CategoryController@sendCategoryOnline',
    'as'   => 'category.sendCategoryOnline'
]);

Route::get('category/receiveCategory', [
    'uses' => 'CategoryController@receiveCategory',
    'as'   => 'category.receiveCategory'
]);

Route::get('customer/sendCustomerOnline', [
    'uses' => 'CustomerController@sendCustomerOnline',
    'as'   => 'customer.sendCustomerOnline'
]);

Route::get('customer/receiveCustomer', [
    'uses' => 'CustomerController@receiveCustomer',
    'as'   => 'customer.receiveCustomer'
]);



Route::get('product/sendOrderOnline', [
    'uses' => 'ProductController@sendOrdersOnline',
    'as'   => 'product.sendOrderOnline'
]);

Route::get('product/receiveOrder', [
    'uses' => 'ProductController@receiveOrders',
    'as'   => 'product.receiveOrder'
]);



Route::get('user/sendUserOnline', [
    'uses' => 'UserController@sendUserOnline',
    'as'   => 'user.sendUserOnline'
]);

Route::get('user/receiveUser', [
    'uses' => 'UserController@receiveUser',
    'as'   => 'user.receiveUser'
]);


Route::group(['prefix' => 'parking'], function() {
    Route::get('', [
        'uses' => 'ProductController@ViewParked',
        'as' => 'parking.index',
        'middleware' => 'auth'
    ]);

});




