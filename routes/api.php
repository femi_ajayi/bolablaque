<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'api/v1'], function() {


    Route::post('product/getProductData', [
        'uses' => 'ProductController@getProductData',
        'as'   => 'product.getProductData'
    ]);

    Route::post('product/sendProductToLocal', [
        'uses' => 'ProductController@sendProductToLocal',
        'as'   => 'product.sendProductToLocal'
    ]);


    Route::post('brand/getBrandData', [
        'uses' => 'BrandController@getBrandData',
        'as'   => 'brand.getBrandData'
    ]);

    Route::post('brand/sendBrandToLocal', [
        'uses' => 'BrandController@sendBrandToLocal',
        'as'   => 'brand.sendBrandToLocal'
    ]);


    Route::post('category/getCategoryData', [
        'uses' => 'CategoryController@getCategoryData',
        'as'   => 'category.getCategoryData'
    ]);

    Route::post('category/sendCategoryToLocal', [
        'uses' => 'CategoryController@sendCategoryToLocal',
        'as'   => 'category.sendCategoryToLocal'
    ]);


    Route::post('customer/getCustomerData', [
        'uses' => 'CustomerController@getCustomerData',
        'as'   => 'customer.getCustomerData'
    ]);

    Route::post('customer/sendCustomerToLocal', [
        'uses' => 'CustomerController@sendCustomerToLocal',
        'as'   => 'customer.sendCustomerToLocal'
    ]);



    Route::post('product/getOrderData', [
        'uses' => 'ProductController@getOrderData',
        'as'   => 'product.getOrderData'
    ]);

    Route::post('product/sendOrderToLocal', [
        'uses' => 'ProductController@sendOrderToLocal',
        'as'   => 'product.sendOrderToLocal'
    ]);



    Route::post('user/getUserData', [
        'uses' => 'UserController@getUserData',
        'as'   => 'user.getUserData'
    ]);

    Route::post('user/sendUserToLocal', [
        'uses' => 'UserController@sendUserToLocal',
        'as'   => 'user.sendUserToLocal'
    ]);



});
