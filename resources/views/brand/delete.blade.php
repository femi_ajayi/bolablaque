@extends('layouts.master')

@section('title')
    Bola Blaque :: Delete Product Brand
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Are you sure you want to delete  this? </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('brand.remove') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong>  Delete Brand  </strong>
                </div>
                <div class="card-body card-block">
                    {{--@include('includes.message-block')--}}
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">Brand Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $brand->name }}" name="name" placeholder="Enter Brand Name..." class="form-control"></div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="id" value="{{ $brand->id }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Delete
                    </button>

                </div>
            </form>
        </div>
    </div>







@endsection



