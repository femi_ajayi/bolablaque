@extends('layouts.master')

@section('title')
    Bola Blaque :: Update Product Category
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Update  Category </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('category.update') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong>  Update Category  </strong>
                </div>
                <div class="card-body card-block">
                    {{--@include('includes.message-block')--}}
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">Category Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $category->name }}" name="name" placeholder="Enter Category Name..." class="form-control"><span class="help-block">Please enter category name</span></div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="id" value="{{ $category->id }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Update
                    </button>

                </div>
            </form>
        </div>
    </div>







@endsection



