@extends('layouts.master')

@section('title')
    Bola Blaque :: Product Category
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Product Category</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><a type="button" class="btn btn-success"
                                              href="{{ route('category.create') }}"> Create New Category </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <form class="form-inline" id="basic-form" action="{{ route('category.categoryName') }}" method="post">
                    <div class="form-group">
                        <input type="text"  class="form-control" placeholder="Search By Category Name"
                               name="query" required>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <button type="button" name="search" onClick="location.href=location.href"  class="btn btn-warning">Refresh</button>
                        @csrf
                    </div>
                </form>
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Category Name</th>
                            <th scope="col">Date Added</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ $category->name }} </td>
                                <td> {{  date('d/m/Y h:i a', strtotime($category->created_at)) }} </td>
                                <td><a  class='btn btn-warning' href="{{ route('category.edit' , ['id' => $category->id]) }}"  ><i class="fa fa-edit"></i></a></td>
                                <td> <a class="btn btn-danger" href="{{ route('category.delete', ['id' => $category->id]) }}"><i class="fa fa-trash"></i></a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            {{ $categories->links() }}
                        </div>
                    </div>
            </div>
        </div>
    </div>







@endsection



