@extends('layouts.master')

@section('title')
    Bola Blaque ::  Sales Reports
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Sales Reports</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        {{--   <li class="active"><a type="button" class="btn btn-success" href="{{ route('customer.create') }}">
                                   Create New Customer </a>
                           </li>--}}
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                    <form  class="form-horizontal" action="{{ route('reports.salesPerson') }}" method="POST" >
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <input type="date" id="startDate" name="startDate" style="width: 250px" placeholder="Start Date"
                                           class="form-control" required>
                                    <input type="date" id="endDate" name="endDate" style="width: 250px" placeholder="End Date"
                                           class="form-control" required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="{{ route('reports.sales') }}" class="btn btn-danger">Refresh</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <p>Total amount for this period ₦{{ number_format($total_sum) }} <br/>
                            Discounted amount for this period ₦{{ number_format($discounted_sum) }}  </p>
                    </div>

                    <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Price</th>
                            <th>Item Qty</th>
                            <th>Served By</th>
                            <th>Date </th>
                            <th></th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ (isset($order->customer_id)) ? $order->customer['full_name'] : "Generic Customer" }}  </td>
                                <td>  ₦{{ number_format($order->total_price) }}</td>
                                <td>{{ $order->quantity }}</td>
                                <td> {{ $order->salesperson }}  </td>
                                <td> {{  date('d/m/Y h:i a', strtotime($order->created_at)) }} </td>
                                <td><a href="{{ route('orders.print' , ['id' => $order->id]) }}"> &nbsp; View Details </a></td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>







@endsection



