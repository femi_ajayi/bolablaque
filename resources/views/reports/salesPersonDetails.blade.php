@extends('layouts.master')

@section('title')
    Bola Blaque ::  Sales Report By SalesPerson
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Sales Report By SalesPerson </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <form class="form-inline" action="{{ route('reports.salesPersonDetails') }}" method="post">

                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Brand</label>
                        <div class="input-group">
                            <div class="input-group-addon">User</div>
                            <select class="form-control"  name="sales_person" style="width: 200px;" required>
                                <option value="">--Select User--</option>
                                @foreach($users as $user){
                                <option
                                    value="{{ $user->first_name ." ". $user->last_name }}">{{  $user->first_name ." ". $user->last_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Start Date</label>
                        <div class="input-group">
                            <div class="input-group-addon">Start Date</div>
                            <input type="date" id="startDate" name="startDate" style="width: 250px" placeholder="Start Date"
                                   class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">End Date</label>
                        <div class="input-group">
                            <div class="input-group-addon">End Date</div>
                            <input type="date" id="endDate" name="endDate" style="width: 250px" placeholder="End Date"
                                   class="form-control" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{{ route('reports.salesPerson') }}" class="btn btn-danger">Refresh</a>
                    @csrf
                </form>
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Price</th>
                            <th>Item Qty</th>
                            <th>Served By</th>
                            <th>Date </th>
                            <th></th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ (isset($order->customer_id)) ? $order->customer['full_name'] : "Generic Customer" }}  </td>
                                <td>  ₦{{ number_format($order->total_price) }}</td>
                                <td>{{ $order->quantity }}</td>
                                <td> {{ $order->salesperson }}  </td>
                                <td> {{  date('d/m/Y h:i a', strtotime($order->created_at)) }} </td>
                                <td><a href="{{ route('orders.print' , ['id' => $order->id]) }}"> &nbsp; View Details </a></td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>







@endsection



