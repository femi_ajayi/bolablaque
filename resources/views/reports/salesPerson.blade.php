@extends('layouts.master')

@section('title')
    Bola Blaque :: Sales Report By SalesPerson
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Sales Report By SalesPerson </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
        @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{ Session::get('fail') }}
            </div>
        @endif
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <div class="col-lg-12">

                    <form class="form-inline" action="{{ route('reports.salesPersonDetails') }}" method="post">

                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Brand</label>
                            <div class="input-group">
                                <div class="input-group-addon">User</div>
                                <select class="form-control"  name="sales_person" style="width: 200px;" required>
                                    <option value="">--Select User--</option>
                                    @foreach($users as $user){
                                    <option
                                        value="{{ $user->first_name ." ". $user->last_name }}">{{  $user->first_name ." ". $user->last_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Start Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">Start Date</div>
                                <input type="date" id="startDate" name="startDate" style="width: 250px" placeholder="Start Date"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">End Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">End Date</div>
                                <input type="date" id="endDate" name="endDate" style="width: 250px" placeholder="End Date"
                                       class="form-control" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="submit" onClick="location.href=location.href" class="btn btn-danger">Refresh</button>
                        @csrf
                    </form>


                    {{--   <form  class="form-horizontal" action="{{ route('brandReportSearch') }}" method="POST" >
                           <div class="row form-group">
                               <div class="col col-md-12">

                                   <div class="form-group">
                                       <label class="sr-only" for="exampleInputAmount">Department</label>
                                       <div class="input-group">
                                           <div class="input-group-addon">Brand</div>
                                           <select class="form-control"  name="department" style="width: 200px;" required>
                                               <option value="">--Select Brand--</option>
                                               @foreach($brands as $brand){
                                               <option
                                                   value="{{ $brand->id }}">{{ $brand->name }}
                                               </option>
                                               @endforeach
                                           </select>
                                       </div>
                                   </div>


                                   <div class="input-group">
                                       <input type="date" id="startDate" name="startDate" style="width: 250px" placeholder="Start Date"
                                              class="form-control" required>
                                       <input type="date" id="endDate" name="endDate" style="width: 250px" placeholder="End Date"
                                              class="form-control" required>
                                       <div class="input-group-btn">
                                           <button type="submit" class="btn btn-primary">Submit</button>
                                           <button type="submit" onClick="location.href=location.href" class="btn btn-danger">Refresh</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           @csrf
                       </form>--}}

                    <div id="orderDetails">

                    </div>






                </div>



            </div>




        </div>
    </div>




    <script>

        let searchReport = "{{ route('reportSearch') }}";

    </script>


@endsection






