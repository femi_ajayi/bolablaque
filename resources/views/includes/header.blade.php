

<body>


<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>bolablaque.png
            </button>
            <a class="navbar-brand" href="{{ route('dashboard') }}"><img src="{{ URL::to('/') }}/lib/images/bolablaque.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="{{ route('dashboard') }}"><img src="{{ URL::to('/') }}/lib/images/B_plain.png" alt="Logo"></a>
            {{--<a class="navbar-brand" href="{{ route('dashboard') }}"><img src="{{ URL::to('/') }}/lib/images/logo.png" alt="Logo"></a>--}}
            {{--<a class="navbar-brand hidden" href="{{ route('dashboard') }}"><img src="{{ URL::to('/') }}/lib/images/logo2.png" alt="Logo"></a>--}}
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>

                <li>
                    <a href="{{ route('orders.create') }}"> <i class="menu-icon ti-tablet"></i> Sell Product </a>
                </li>

                <li>
                    <a href="{{ route('orders.createName') }}"> <i class="menu-icon ti-tablet"></i> Sell Product(By Name) </a>
                </li>

              {{--  <li>
                    <a href="{{ route('parking.index') }}"> <i class="menu-icon ti-tablet"></i> Parkings </a>
                </li>--}}

                <li>
                    <a href="{{ route('product.index') }}"> <i class="menu-icon ti-blackboard"></i>Products </a>
                </li>

                <li>
                    <a href="{{ route('brand.index') }}"> <i class="menu-icon ti-server"></i>Products Brand </a>
                </li>

                <li>
                    <a href="{{ route('category.index') }}"> <i class="menu-icon ti-email"></i> Categories  </a>
                </li>

                <li>
                    <a href="{{ route('customer.index') }}"> <i class="menu-icon ti-harddrives"></i> Customers </a>
                </li>

                <li>
                    <a href="{{ route('orders.sales') }}"> <i class="menu-icon ti-calendar"></i> Sales </a>
                </li>

                <li>
                    <a href="{{ route('syncData') }}"> <i class="menu-icon ti-calendar"></i> Sync </a>
                </li>

                @if( Auth::user()->role != "User" )
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ti-archive"></i>Reports</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('reports.sales') }}"> Sales Report</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="{{ route('reports.salesPerson') }}"> Reports(Salesperson)</a></li>

{{--                        <li><i class="menu-icon fa fa-th"></i><a href="#"> Reports(Brand)</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="#"> Reports(Category)</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="#"> Reports(Customers)</a></li>
                        <li><i class="menu-icon fa fa-th"></i><a href="#">All Customers</a></li>--}}

                    </ul>
                </li>
                @endif

                <li>
                    <a href="{{ route('UserAccount.index') }}"> <i class="menu-icon ti-agenda"></i> User Management </a>
                </li>


            </ul>
        </div>
    </nav>
</aside>



<div id="right-panel" class="right-panel">

    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">

                </div>
            </div>

            <div class="col-sm-5">
                <div class="user-area dropdown float-right">

                    @if (getBirthdays() >= 1)
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="profile-text">
                        <a href="{{ route('customer.birthdays') }}"><i class="lnr lnr-alarm"></i> <span class="badge bg-danger" style="color: white"> {{ getBirthdays() }} </span> Birthdays </a>
                        </span>
                    </a>
                    @endif

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="profile-text">Hello,  {{ Auth::user()->first_name ." ". Auth::user()->last_name }} ! </span>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="{{ route('UserAccount.profile', ['id' => Auth::user()->id] ) }}"><i class="fa fa-user"></i> My Profile</a>

{{--                        <a class="nav-link" href="#"><i class="fa fa-user"></i> Notifications <span class="count">13</span></a>
                        <a class="nav-link" href="#"><i class="fa fa-cog"></i> Settings</a>--}}

                        <a class="nav-link" href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                    </div>
                </div>



            </div>
        </div>

    </header>

