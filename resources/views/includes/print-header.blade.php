<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title') </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ URL::to('lib/apple-icon.png') }}">
    <link rel="shortcut icon" href="{{ URL::to('lib/favicon.ico') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/selectFX/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/jqvmap/dist/jqvmap.min.css') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.theme.min.css') }}" >


    <link rel="stylesheet" href="{{ URL::to('lib/assets/css/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>


<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="{{ route('dashboard') }}"> Bolablaque Beauty </a>



</nav>




@yield('content')


<script src="{{ URL::to('lib/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/main.js') }}"></script>

<script src="{{ URL::to('lib/src/js/custom.js') }}"></script>


<script src="{{ URL::to('lib/vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/dashboard.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/widgets.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>

<script src="{{ URL::to('lib/jquery-ui/jquery-ui.min.js') }}" ></script>


<script>

    (function ($) {
        "use strict";



        $('#printReceipt').click(function () {
            // printPageArea('#printArea')
            // document.querySelector('#printArea').print();
            window.print();
        });






    })(jQuery);


</script>

</body>

</html>
