@if(count($errors) > 0)
    <div class="row">

        {{--     <div class="col-md-6 col-md-offset-2 error">--}}

        <div class="error">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                @foreach($errors->all() as $error)
                    {{$error }}  <br/>
                @endforeach
            </div>


        </div>
    </div>
@endif
@if(Session::has('message'))
    <div class="row">
       {{-- <div class="col-md-4 col-md-offset-4 success">--}}
            {{Session::get('message')}}
       {{-- </div>--}}
    </div>

@endif


