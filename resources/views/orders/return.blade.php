@extends('layouts.master')

@section('title')
    Bola Blaque :: Print Receipts
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Print Receipts</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">

                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            {{-- <div class="card-header">
                 <strong class="card-title">Stripped Table</strong>

                 <strong style="float: right" class="card-title">Put Search Box Here</strong>
             </div>--}}
            <div class="card-body">

                <a href="{{ route('orders.sales') }}" style="font-size: x-large" id="bill">&laquo; Back</a>


                <div class="row">


                    <div class="col-md-2">
                    </div>


                    <div class="col-md-8" id="printArea">
                        <h1 style=" text-align: center"><b>Bolablaque Beauty Store</b></h1>
                        <p style=" text-align: center"><small>15 Victoria Arobieke Lekki Phase 1. Lagos <br/>
                                Tel: 08086640420, Email: bolablaque@gmail.com
                                <br/><b>All items are non-exchangeable and non-refundable</b></small></p>

                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th>Item(s)</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sub-Total</th>
                                <th></th>


                            </tr>
                            </thead>

                            <tbody>
                            @foreach($carts->items as $cart)
                                <tr>
                                    <td>  {{ $cart['name'] }} </td>
                                    <td>  {{ $cart['qty'] }} </td>
                                    <td>  ₦{{ number_format($cart['selling_price']) }}</td>
                                    <td>  ₦{{ number_format($cart['total_price'])  }} </td>
                                    <td> <a href="{{ route('orders.postReturn') }}">Return</a> </td>
                            @endforeach
                            </tbody>


                            <tr>
                                <td colspan='3' class='text-left'>Total Price : </td>
                                <td colspan='2' class='text-left'>₦{{ number_format($order->total_price)  }}</td>
                            </tr>

                            @if($order->total_price > $order->amount_paid)
                                <tr>
                                    <td colspan='3' class='text-left'>Discount </td>
                                    <td colspan='2' class='text-left'>₦{{number_format($order->total_price - $order->amount_paid)  }}</td>
                                </tr>
                                <tr>
                                    <td colspan='3' class='text-left'>Amount Paid :</td>
                                    <td colspan='2' class='text-left'>₦{{ number_format($order->amount_paid)  }}</td>
                                </tr>
                            @endif



                        </table>
                        <table>

                            <tr>
                                <td><b>Served By</b></td>
                                <td style='padding-left: 100px'> {{ $order->salesperson }}    </td>
                            </tr>

                            <tr>
                                <td><b>Payment Mode</b></td>
                                <td style='padding-left: 100px'> {{ $order->payment_type }}    </td>
                            </tr>

                            <tr>
                                <td><b>Date</b></td>
                                <td style='padding-left: 100px'>
                                    {{ date('d/m/Y h:i a', strtotime($order->created_at)) }}
                                </td>
                            </tr>

                        </table>



                    </div>




                    <div class="col-md-2">

                    </div>

                </div>


            </div>
        </div>
    </div>







@endsection



