@extends('layouts.master')

@section('title')
    Bola Blaque :: Print Receipts
@endsection


@section('content')

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Print Receipts</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">

                </ol>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-12">
    <div class="card">
        {{-- <div class="card-header">
             <strong class="card-title">Stripped Table</strong>

             <strong style="float: right" class="card-title">Put Search Box Here</strong>
         </div>--}}
        <div class="card-body">

            <a href="{{ route('orders.sales') }}" style="font-size: x-large" id="bill">&laquo; Back</a>


            <div class="row">


                <div class="col-md-3">
                </div>


                <div class="col-md-6" id="printArea">
                    <h1 style=" text-align: center"><b>Bolablaque Beauty Store</b></h1>
                    <p style=" text-align: center"><small>15 Victoria Arobieke Lekki Phase 1. Lagos <br/>
                            Tel: 08086640420, Email: bolablaque@gmail.com
                            <br/><b>All items are non-exchangeable and non-refundable</b></small></p>

                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Item(s)</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Sub-Total</th>


                        </tr>
                        </thead>

                        <tbody>
                        @foreach($carts->items as $cart)
                            <tr>
                                <td>  {{ $cart['name'] }} </td>
                                <td>  {{ $cart['qty'] }} </td>
                                <td>  ₦{{ number_format($cart['selling_price']) }}</td>
                                <td>  ₦{{ number_format($cart['total_price'])  }} </td>
                        @endforeach
                        </tbody>


                        <tr>
                            <td colspan='3' class='text-left'>Total Price : </td>
                            <td colspan='2' class='text-left'>₦{{ number_format($order->total_price)  }}</td>
                        </tr>

                        @if($order->total_price > $order->amount_paid)
                            <tr>
                                <td colspan='3' class='text-left'>Discount </td>
                                <td colspan='2' class='text-left'>₦{{number_format($order->total_price - $order->amount_paid)  }}</td>
                            </tr>
                            <tr>
                                <td colspan='3' class='text-left'>Amount Paid :</td>
                                <td colspan='2' class='text-left'>₦{{ number_format($order->amount_paid)  }}</td>
                            </tr>
                        @endif


                        {{--         <tr>
                                     <td colspan='3' class='text-left'><strong>Total Price : </strong></td>
                                     <td colspan='2' class='text-left'><strong><?php echo "₦$order->total_price"; ?></strong></td>
                                 </tr>
                                 <?php if ($order->total_price > $order->amount_paid) { ?>
                                 <tr>
                                     <td colspan='3' class='text-left'><strong>Discount </strong></td>
                                     <td colspan='2' class='text-left'><strong><?php $discount = $order->total_price - $order->amount_paid;
                                             echo "₦$discount";  ?></strong></td>
                                 </tr>
                                 <?php } ?>
                                 <?php if ($order->total_price > $order->amount_paid) { ?>
                                 <tr>
                                     <td colspan='3' class='text-left'><strong>Amount Paid </strong></td>
                                     <td colspan='2' class='text-left'><strong><?php echo "₦$order->amount_paid";  ?></strong></td>
                                 </tr>
                                 <?php } ?>--}}

                    </table>
                    <table>

                        <tr>
                            <td><b>Served By</b></td>
                            <td style='padding-left: 100px'> {{ $order->salesperson }}    </td>
                        </tr>

                        <tr>
                            <td><b>Payment Mode</b></td>
                            <td style='padding-left: 100px'> {{ $order->payment_type }}    </td>
                        </tr>

                        <tr>
                            <td><b>Date</b></td>
                            <td style='padding-left: 100px'>
                                {{ date('d/m/Y h:i a', strtotime($order->created_at)) }}
                            </td>
                        </tr>

                    </table>

                    <h4>---------------------------------------------------------------------</h4>
                    <p style="font-size: small; text-align: center"><i>...bringing beauty to life</i>
                    </p>

                    <h4>---------------------------------------------------------------------</h4>

                    <form method="post" action="{{ route('postReturn') }}">
                        <input type="button" value="Print Receipt" id="printReceipt" class="btn btn-success"/>
                        @if (Auth::user()->role != "User")
                            <input value="{{ $order->id }}" name="order_id" hidden>
                            <input type="submit" value="Return Items"  class="btn btn-danger"/>
                            @csrf
                        @endif
                    </form>

                </div>




                <div class="col-md-3">

                </div>

            </div>


        </div>
    </div>
</div>







@endsection



