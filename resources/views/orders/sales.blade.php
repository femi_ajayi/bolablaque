@extends('layouts.master')

@section('title')
    Bola Blaque :: Recent Sales
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Recent Sales</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                     {{--   <li class="active"><a type="button" class="btn btn-success" href="{{ route('customer.create') }}">
                                Create New Customer </a>
                        </li>--}}
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
{{--                <strong class="card-title">Stripped Table</strong>

                <strong style="float: right" class="card-title">Put Search Box Here</strong>--}}
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Price</th>
                            <th>Item Qty</th>
                            <th>Served By</th>
                            <th>Date </th>
                            <th></th>
                            <th></th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ (isset($order->customer_id)) ? $order->customer['full_name'] : "Generic Customer" }}  </td>
                                <td>  ₦{{ number_format($order->total_price) }}</td>
                                <td>{{ $order->quantity }}</td>
                                <td> {{ $order->salesperson }}  </td>
                                <td> {{  date('d/m/Y h:i a', strtotime($order->created_at)) }} </td>
                               {{-- <td><a href="{{ route('orders.print' , ['id' => $order->id]) }}"> &nbsp; View Details </a></td>--}}
                                <td><a href="{{ route('orders.re-print' , ['id' => $order->id]) }}"> &nbsp; Re-print </a></td>
                                <td><a href="{{ route('mailReceipt', ['id' => $order->id]) }}">Mail Receipt</a> </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>







@endsection



