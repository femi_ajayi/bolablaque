@extends('layouts.master')

@section('title')
    Bola Blaque :: Mail Receipts
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Mail Receipts</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">

                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            {{-- <div class="card-header">
                 <strong class="card-title">Stripped Table</strong>

                 <strong style="float: right" class="card-title">Put Search Box Here</strong>
             </div>--}}
            <div class="card-body">

                <a href="{{ route('orders.sales') }}" style="font-size: x-large" id="bill">&laquo; Back</a>


                <div class="row">


                    <div class="col-md-3">
                    </div>

                    <div class="col-md-6" id="printArea">
                        @if(Session::has('info'))
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="alert alert-info">{{ Session::get('info') }}</p>
                                </div>
                            </div>
                        @endif
                        <h1 style=" text-align: center"><b>Bolablaque Beauty Store</b></h1>
                        <p style=" text-align: center"><small>15 Victoria Arobieke Lekki Phase 1. Lagos <br/>
                                Tel: 08086640420, Email: bolablaque@gmail.com
                                <br/><b>All items are non-exchangeable and non-refundable</b></small></p>

                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th>Item(s)</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sub-Total</th>


                            </tr>
                            </thead>

                            <tbody>
                            @foreach($carts->items as $cart)
                                <tr>
                                    <td>  {{ $cart['name'] }} </td>
                                    <td>  {{ $cart['qty'] }} </td>
                                    <td>  ₦{{ number_format($cart['selling_price']) }}</td>
                                    <td>  ₦{{ number_format($cart['total_price'])  }} </td>
                            @endforeach
                            </tbody>


                            <tr>
                                <td colspan='3' class='text-left'>Total Price : </td>
                                <td colspan='2' class='text-left'>₦{{ number_format($order->total_price)  }}</td>
                            </tr>

                            @if($order->total_price > $order->amount_paid)
                                <tr>
                                    <td colspan='3' class='text-left'>Discount </td>
                                    <td colspan='2' class='text-left'>₦{{number_format($order->total_price - $order->amount_paid)  }}</td>
                                </tr>
                                <tr>
                                    <td colspan='3' class='text-left'>Amount Paid :</td>
                                    <td colspan='2' class='text-left'>₦{{ number_format($order->amount_paid)  }}</td>
                                </tr>
                            @endif


                        </table>
                        <table>

                            <tr>
                                <td><b>Served By</b></td>
                                <td style='padding-left: 100px'> {{ $order->salesperson }}    </td>
                            </tr>

                            <tr>
                                <td><b>Payment Mode</b></td>
                                <td style='padding-left: 100px'> {{ $order->payment_type }}    </td>
                            </tr>

                            <tr>
                                <td><b>Date</b></td>
                                <td style='padding-left: 100px'>
                                    {{ date('d/m/Y h:i a', strtotime($order->created_at)) }}
                                </td>
                            </tr>

                        </table>

                        <h4>---------------------------------------------------------------------</h4>
                        <p style="font-size: small; text-align: center"><i>...bringing beauty to life</i>
                        </p>

                        <h4>---------------------------------------------------------------------</h4>

                        <form method="post" action="{{ route('postMailReceipt') }}">
                                <input value="{{--{{ $carts->items }}--}}"   name="carts" hidden >
                                <input value="{{ $order->id  }}" name="order" hidden >
                                 <input class="form-control" value="{{ $order->customer['email'] }}" name="receiver_email" placeholder="Enter Customer's Email address" required >
                            <h4>---------------------------------------------------------------------</h4>
                                <input type="submit" value="Mail Receipt To Customer"  class="btn btn-danger"/>
                                @csrf

                        </form>

                    </div>




                    <div class="col-md-3">

                    </div>

                </div>


            </div>
        </div>
    </div>







@endsection



