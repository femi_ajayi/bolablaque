<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title') </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ URL::to('lib/apple-icon.png') }}">
    <link rel="shortcut icon" href="{{ URL::to('lib/favicon.ico') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/selectFX/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/jqvmap/dist/jqvmap.min.css') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.theme.min.css') }}" >


    <link rel="stylesheet" href="{{ URL::to('lib/assets/css/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>


<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="{{ route('dashboard') }}"> Bolablaque Beauty </a>



</nav>









<div class="col-lg-12">
    <div class="card">

        <div class="card-body">

            <a href="{{ route('orders.sales') }}" style="font-size: x-large" id="bill">&laquo; Back</a>


            <div class="row">


                <div class="col-md-3">
                </div>


                <div class="col-md-6" id="printArea">
                    <h1 style=" text-align: center"><b>Bolablaque Beauty Store</b></h1>
                    <p style=" text-align: center"><small>15 Victoria Arobieke Lekki Phase 1. Lagos <br/>
                            Tel: 08086640420, Email: bolablaque@gmail.com
                            <br/><b>All items are non-exchangeable and non-refundable</b></small></p>

                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Item(s)</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Sub-Total</th>


                        </tr>
                        </thead>

                        <tbody>
                        @foreach($carts->items as $cart)
                            <tr>
                                <td>  {{ $cart['name'] }} </td>
                                <td>  {{ $cart['qty'] }} </td>
                                <td>  ₦{{ number_format($cart['selling_price']) }}</td>
                                <td>  ₦{{ number_format($cart['total_price'])  }} </td>
                        @endforeach
                        </tbody>


                        <tr>
                            <td colspan='3' class='text-left'>Total Price : </td>
                            <td colspan='2' class='text-left'>₦{{ number_format($order->total_price)  }}</td>
                        </tr>

                        @if($order->total_price > $order->amount_paid)
                            <tr>
                                <td colspan='3' class='text-left'>Discount </td>
                                <td colspan='2' class='text-left'>₦{{number_format($order->total_price - $order->amount_paid)  }}</td>
                            </tr>
                            <tr>
                                <td colspan='3' class='text-left'>Amount Paid :</td>
                                <td colspan='2' class='text-left'>₦{{ number_format($order->amount_paid)  }}</td>
                            </tr>
                        @endif



                    </table>
                    <table>

                        <tr>
                            <td><b>Served By</b></td>
                            <td style='padding-left: 100px'> {{ $order->salesperson }}    </td>
                        </tr>

                        <tr>
                            <td><b>Payment Mode</b></td>
                            <td style='padding-left: 100px'> {{ $order->payment_type }}    </td>
                        </tr>

                        <tr>
                            <td><b>Date</b></td>
                            <td style='padding-left: 100px'>
                                {{ date('d/m/Y h:i a', strtotime($order->created_at)) }}
                            </td>
                        </tr>

                    </table>

                    <h4>---------------------------------------------------------------------</h4>
                    <p style="font-size: small; text-align: center"><i>...bringing beauty to life</i>
                    </p>

                    <h4>---------------------------------------------------------------------</h4>

                    <form method="post" action="{{ route('postReturn') }}">
                        <input type="button" value="Print Receipt" id="printReceipt" class="btn btn-success"/>
                        @if (Auth::user()->role != "User")
                            <input value="{{ $order->id }}" name="order_id" hidden>
                            <input type="submit" value="Return Items"  class="btn btn-danger"/>
                            @csrf
                        @endif
                    </form>

                </div>




                <div class="col-md-3">

                </div>

            </div>


        </div>
    </div>
</div>

















<script src="{{ URL::to('lib/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/main.js') }}"></script>

<script src="{{ URL::to('lib/src/js/custom.js') }}"></script>


<script src="{{ URL::to('lib/vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/dashboard.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/widgets.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>

<script src="{{ URL::to('lib/jquery-ui/jquery-ui.min.js') }}" ></script>


<script>

    (function ($) {
        "use strict";



        $('#printReceipt').click(function () {
            // printPageArea('#printArea')
            // document.querySelector('#printArea').print();
            window.print();
        });






    })(jQuery);


</script>

</body>

</html>
