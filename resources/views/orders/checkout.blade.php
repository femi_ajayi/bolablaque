@extends('layouts.master')

@section('title')
    Bola Blaque :: Save Order
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Sell Products </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
        @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{ Session::get('fail') }}
            </div>
        @endif
    </div>


    <div class="col-lg-12">
        <div class="card">

            <div class="card-body">


                <div class="col-lg-6">
                    <a style="font-size: large" href="{{ route('orders.createName') }}">Back</a> <br/>
                    <label>Enter Customer's details or just proceed with general sale</label>
                    <form class="form-horizontal" id="customer_search">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <input type="text" id="phone_number" name="phone"  placeholder="Enter Customer's Phone No."
                                           class="form-control add_to_bill" autofocus required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                    <form class="form-horizontal" id="customer_search_name">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <input type="text" id="cus_name" name="cus_name"  placeholder="Enter Customer's Name"
                                           class="form-control add_to_bill"  required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <form action="{{ route('orders.checkout') }}" method="post">
                <div class="col-lg-6" id="customerInfo">

                </div>

                    <div class="col-lg-6" id="customerInfoName">

                    </div>



                <div class="col-lg-12">
                    @if (Session::has('cart'))

                    <div class="user-cart">
                        <table class="table table-bordered table-condensed table-hover">

                                <thead>
                                <tr>
                                    <th>Item(s)</th>
                                    <th>Quantity</th>
                                    <th> Unit Price</th>
                                    <th>Sub-Total</th>
                                  {{--  <th></th>
                                    <th></th>
                                    <th></th>--}}
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($cart as $item)
                                    <tr>
                                        <td> {{ $item['name'] }}</td>
                                        <td><input type='number' class='form-control inp_numb' min='1'
                                                   value='{{  $item['qty']  }}'
                                                   style='width:70px;' readonly></td>
                                        <td>₦{{ number_format($item['selling_price'] ) }}</td>
                                        <td>₦{{ number_format($item['total_price'])   }}</td>
                                    {{--    <td><span data-id=$product[id] class='ti-plus inc_cart'></span></td>
                                        <td><span data-id=$product[id] class='ti-minus dec_cart'></span></td>
                                        <td><span data-id=$product[id] class='ti-close del_cart'></span></td>--}}
                                    </tr>
                                @endforeach
                                <tr>
                                    <th> Total Price </th>
                                    <td colspan="3">
                                      <span style="float: right">₦{{ number_format($total) }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th> Discount </th>
                                    <td colspan="3">
                                        <input type="text" name="discount" placeholder="Discount" class="form-control" >
                                    </td>
                                </tr>
                                <tr>
                                    <th>Payment Type</th>
                                    <td colspan="3">
                                        <select class="form-control" name="payment" required>
                                            <option value="">--Select Payment Type--</option>
                                            <option value="Cash">Cash</option>
                                            <option value="Debit Card">Debit Card</option>
                                            <option value="Transfer">Transfer</option>
                                            <option value="Cheque">Cheque</option>
                                        </select>
                                       {{-- <div class="form-check">
                                            <div class="checkbox">
                                                <label for="checkbox1" class="form-check-label ">
                                                    <input type="checkbox" id="checkbox1" name="Cash" value="Cash" class="form-check-input">Cash
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="checkbox2" class="form-check-label ">
                                                    <input type="checkbox" id="checkbox2" name="Debit Card" value="Debit Card" class="form-check-input"> Debit Card
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="checkbox3" class="form-check-label ">
                                                    <input type="checkbox" id="checkbox3" name="Transfer" value="Transfer" class="form-check-input"> Transfer
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="checkbox3" class="form-check-label ">
                                                    <input type="checkbox" id="checkbox3" name="Cheque" value="Cheque" class="form-check-input"> Cheque
                                                </label>
                                            </div>
                                        </div>--}}
                                    </td>
                                </tr>
                                </tbody>



                        </table>

                    </div>

                    @endif
                    @csrf
                        <button  type="submit"  class="btn btn-danger">Final Save</button>

                </div>
                </form>






            </div>


        </div>
    </div>


    <script>

        let searchCustomer     = "{{ route('customerSearch') }}";
        let searchCustomerName = "{{ route('customerSearchName') }}";

    </script>


@endsection






