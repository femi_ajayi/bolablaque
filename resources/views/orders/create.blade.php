@extends('layouts.master')

@section('title')
    Bola Blaque :: Sell Product
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Sell Products </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
        @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{ Session::get('fail') }}
            </div>
        @endif
    </div>


{{--    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <div class="col-lg-6">


                    <form action="{{ route('ordersCreate') }}" method="post" class="form-horizontal" id="barcode_search">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <input type="text" id="bar_num" name="barcode"  placeholder="Scan Product"
                                           class="form-control add_to_bill" autofocus required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="submit" onClick="location.href=location.href" class="btn btn-danger">Refresh</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>


                </div>



            </div>

             <div class="card-body">

                <div id="myCart" >

                </div>

            </div>


        </div>
    </div>--}}



        <div class="card">
            <div class="card-body">

                <div class="col-md-5">


                    <form action="{{ route('ordersCreate') }}" method="post" class="form-horizontal" id="barcode_search">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <input type="text" id="bar_num" name="barcode"  placeholder="Scan Product"
                                           class="form-control add_to_bill" autofocus required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="submit" onClick="location.href=location.href" class="btn btn-danger">Refresh</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>

          {{--          <form action="{{ route('ordersCreate') }}" method="post" class="form-inline" id="formSearch" >
                        <div class="form-group">
                            <input  type="text" autocomplete="off"  class="form-control" placeholder="Name Of Product"
                                   name="txtProduct" id="txtProduct" >
                            <button type="submit" class="btn btn-info">Search</button>
                        </div>
                        <div id="drugItems">
                        </div>
                        @csrf
                    </form>--}}







                </div>

                <div class="col-md-7">

                    <div id="myCart" >

                    </div>

                </div>

            </div>

        </div>




    <script>

        let cartIncrease = "{{ route('ordersIncrease') }}";
        let cartDecrease = "{{ route('ordersDecrease') }}";
        let cartDelete   = "{{ route('ordersDelete') }}";

    </script>



@endsection






