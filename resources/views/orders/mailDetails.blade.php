<!DOCTYPE html>
<html>
<title>Mail Receipts<</title>
<body>
<div class="col-md-6" id="printArea">
    <h1 ><b>Bolablaque Beauty Store</b></h1>
    <p ><small>15 Victoria Arobieke Lekki Phase 1. Lagos <br/>
            Tel: 08086640420, Email: bolablaque@gmail.com
            <br/><b>All items are non-exchangeable and non-refundable</b></small></p>


        <table class="table table-bordered table-condensed table-hover">
            <thead>
            <tr>
                <th>Item(s)</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Sub-Total</th>


            </tr>
            </thead>

            <tbody>

            @foreach($carts->items as $cart)
                <tr>
                    <td>  {{ $cart['name'] }} </td>
                    <td>  {{ $cart['qty'] }} </td>
                    <td>  ₦{{ number_format($cart['selling_price']) }}</td>
                    <td>  ₦{{ number_format($cart['total_price'])  }} </td>
            @endforeach
            </tbody>


            <tr>
                <td colspan='3' class='text-left'>Total Price : </td>
                <td colspan='2' class='text-left'>₦{{ number_format($order->total_price)  }}</td>
            </tr>

            @if($order->total_price > $order->amount_paid)
                <tr>
                    <td colspan='3' class='text-left'>Discount </td>
                    <td colspan='2' class='text-left'>₦{{number_format($order->total_price - $order->amount_paid)  }}</td>
                </tr>
                <tr>
                    <td colspan='3' class='text-left'>Amount Paid :</td>
                    <td colspan='2' class='text-left'>₦{{ number_format($order->amount_paid)  }}</td>
                </tr>
            @endif


        </table>
        <table>

            <tr>
                <td><b>Served By</b></td>
                <td style='padding-left: 100px'> {{ $order->salesperson }}    </td>
            </tr>

            <tr>
                <td><b>Payment Mode</b></td>
                <td style='padding-left: 100px'> {{ $order->payment_type }}    </td>
            </tr>

            <tr>
                <td><b>Date</b></td>
                <td style='padding-left: 100px'>
                    {{ date('d/m/Y h:i a', strtotime($order->created_at)) }}
                </td>
            </tr>

        </table>

        <h4>---------------------------------------------------------------------</h4>
        <p style="font-size: small;"><i>...bringing beauty to life</i>
        </p>

        <h4>---------------------------------------------------------------------</h4>




</div>

</body>
</html>






