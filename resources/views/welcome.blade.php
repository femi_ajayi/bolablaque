<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BolaBlaque Beauty :: Welcome  </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ URL::to('lib/apple-icon.png') }}" >
    <link rel="shortcut icon" href="{{ URL::to('lib/favicon.ico') }}" >


    <link rel="stylesheet" href="{{ URL::to('lib/vendors/bootstrap/dist/css/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/font-awesome/css/font-awesome.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/themify-icons/css/themify-icons.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/flag-icon-css/css/flag-icon.min.css') }}"  >
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/selectFX/css/cs-skin-elastic.css') }}" >

    <link rel="stylesheet" href="{{ URL::to('lib/assets/css/style.css') }}" >

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body class="bg-default">


<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                <a href="#">
                    <img class="align-content" src="{{ URL::to('/') }}lib/images/logo.png" alt="">
                </a>
            </div>
            <div class="login-form">
                <div class="form-group">
                    @include('includes.message-block')
                </div>

                @if(Session::has('fail'))
                    <div class="alert alert-danger">
                        {{ Session::get('fail') }}
                    </div>
                @endif
                <form method="post" action="{{ route('login') }}">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} ">
                        <label>Email address</label>
                        <input type="email" class="form-control" name="email" value="{{ Request::old('email') }}"  placeholder="Email">
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }} ">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" value="{{ Request::old('password') }}" placeholder="Password">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                        <label class="pull-right">
                            <a href="#">Forgotten Password?</a>
                        </label>

                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                    <div class="register-link m-t-15 text-center">
                        <p>Don't have account ? <a href="#"> Meet the admin</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="{{ URL::to('lib/vendors/jquery/dist/jquery.min.js') }}" ></script>
<script src="{{ URL::to('lib/vendors/popper.js/dist/umd/popper.min.js') }}" ></script>
<script src="{{ URL::to('lib/vendors/bootstrap/dist/js/bootstrap.min.js') }}" ></script>
<script src="{{ URL::to('lib/assets/js/main.js') }}" ></script>


</body>

</html>
