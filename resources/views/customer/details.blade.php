<div class="col-lg-8">
    <label>Enter Customer's Phone No. or just proceed with general sale</label>
    <form action="{{ route('ordersCreate') }}" method="post" class="form-horizontal" id="customer_search">
        <div class="row form-group">
            <div class="col col-md-12">
                <div class="input-group">
                    <input type="text" id="phone_number" name="phone"  placeholder="Enter Customer's Phone No."
                           class="form-control add_to_bill" value="{{ $customer->full_name }}" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
