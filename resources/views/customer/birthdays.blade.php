@extends('layouts.master')

@section('title')
    Bola Blaque :: Customers
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Customers</h1>
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">

            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Customer Name</th>
                            <th>Phone No.</th>
                            <th>Birthdate</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ $customer->full_name }} </td>
                                <td> {{ $customer->phone_no }} </td>
                                <td> {{  date('d-M', strtotime($customer->birthdate)) }} </td>
                                <td><a class='btn btn-info'  href="{{ route('customer.view' , ['id' => $customer->id]) }}" > <i class="fa fa-info"></i> </a></td>
                                <td><a  class='btn btn-warning' href="{{ route('customer.edit' , ['id' => $customer->id]) }}"  ><i class="fa fa-edit"></i></a></td>
                                <td> <a class="btn btn-danger" href="{{ route('customer.delete', ['id' => $customer->id]) }}"><i class="fa fa-trash"></i></a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
         {{--       <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $customers->links() }}
                    </div>
                </div>--}}
            </div>
        </div>
    </div>







@endsection



