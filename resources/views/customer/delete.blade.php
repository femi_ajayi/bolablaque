@extends('layouts.master')

@section('title')
    BolaBlaque Beauty :: Delete Customer
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Customer   </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">
            @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
            @endif
            <form action="{{ route('customer.remove') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong> Are you sure you want to delete this? </strong>
                </div>
                <div class="card-body card-block">


                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $customer->full_name }}" name="name" placeholder="Enter Customer Name..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Phone No.</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $customer->phone_no }}" name="phone_no" placeholder="Enter Phone Number ..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Contact Address</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $customer->address }}" name="address" placeholder="Enter Contact Address..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Birth Date </label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $customer->birthdate }}" id="dob" name="birthdate" placeholder="Enter Birthdate..." class="form-control"></div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="id" value="{{ $customer->id }}">
                    @csrf {{-- {{ csrf_field() }}--}}
                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Delete
                    </button>
                </div>
            </form>
        </div>
    </div>







@endsection



