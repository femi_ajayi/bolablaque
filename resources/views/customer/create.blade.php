@extends('layouts.master')

@section('title')
    BolaBlaque Beauty :: Register Customer
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Customer </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">
            @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
            @endif
            <form action="{{ route('customer.create') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong> Register Customer </strong>
                </div>
                <div class="card-body card-block">


                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('name') }}" name="name"
                                                            placeholder="Enter Customer Name..." class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="email" class=" form-control-label"> Email</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('email') }}" name="email"
                                                            placeholder="Enter Customer Email..." class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Phone No.</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('phone_no') }}"
                                                            name="phone_no" placeholder="Enter Phone Number ..."
                                                            class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Contact Address</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('address') }}"
                                                            name="address" placeholder="Enter Contact Address..."
                                                            class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Birth Date </label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('birthdate') }}" id="dob"
                                                            name="birthdate" placeholder="Enter Birthdate..."
                                                            class="form-control"></div>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf {{-- {{ csrf_field() }}--}}
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                </div>
            </form>
        </div>
    </div>







@endsection



