@extends('layouts.master')

@section('title')
    BolaBlaque Beauty :: View Customer Information
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Customer Details </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">

            <div class="card-header">
                <strong> View Customer Information </strong>
            </div>
            <div class="card-body card-block">


                <div class="row form-group">
                    <div class="col col-md-3"><label for="name" class=" form-control-label"> Name</label></div>
                    <div class="col-12 col-md-9"><input type="text" value="{{ $customer->full_name }}" name="name"
                                                        placeholder="Enter Customer Name..." class="form-control"></div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="email" class=" form-control-label"> Email</label></div>
                    <div class="col-12 col-md-9"><input type="text" value="{{ $customer->email }}" name="email"
                                                        placeholder="Enter Customer Email..." class="form-control">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="name" class=" form-control-label"> Phone No.</label></div>
                    <div class="col-12 col-md-9"><input type="text" value="{{ $customer->phone_no }}" name="phone_no"
                                                        placeholder="Enter Phone Number ..." class="form-control"></div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="name" class=" form-control-label"> Contact Address</label>
                    </div>
                    <div class="col-12 col-md-9"><input type="text" value="{{ $customer->address }}" name="address"
                                                        placeholder="Enter Contact Address..." class="form-control">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="name" class=" form-control-label"> Birth Date </label></div>
                    <div class="col-12 col-md-9"><input type="text" value="{{ $customer->birthdate }}" id="dob"
                                                        name="birthdate" placeholder="Enter Birthdate..."
                                                        class="form-control"></div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                       {{-- <th>#</th>--}}
                        <th>Customer Name</th>
                        <th>Price</th>
                        <th>Item Qty</th>
                        <th>Served By</th>
                        <th>Date </th>
                        {{-- <th></th>--}}
                        <th></th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                           {{-- <td>{{ $index++ }}</td>--}}
                            <td> {{ (isset($order->customer_id)) ? $order->customer['full_name'] : "Generic Customer" }}  </td>
                            <td>  ₦{{ number_format($order->total_price) }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td> {{ $order->salesperson }}  </td>
                            <td> {{  date('d/m/Y h:i a', strtotime($order->created_at)) }} </td>
                            {{-- <td><a href="{{ route('orders.print' , ['id' => $order->id]) }}"> &nbsp; View Details </a></td>--}}
                            <td><a href="{{ route('customer.orderDetails' , ['id' => $order->customer_id]) }}"> &nbsp; View details</a></td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>







@endsection



