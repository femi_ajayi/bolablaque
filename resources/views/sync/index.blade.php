@extends('layouts.master')

@section('title')
    Bola Blaque :: Sync Data
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Sync Data </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
        @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{ Session::get('fail') }}
            </div>
        @endif
    </div>



    <div class="card">
        <div class="card-body">

            <div class="col-md-6">

                <h2>Send Data Online</h2>

                <form class="form-inline" action="{{ route('postSyncData') }}" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Synchronize</label>
                        <div class="input-group">
                            <div class="input-group-addon">Synchronize</div>
                            <select class="form-control"  name="sync_item" style="width: 200px;" required>
                                <option value="">--Select--</option>
                                <option value="product">Product</option>
                                <option value="brand">Brand</option>
                                <option value="category">Category</option>
                                <option value="orders">Orders</option>
                                <option value="customers">Customers</option>
                                <option value="users">Users</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" name="send_data" class="btn btn-success"> Synchronize </button>
                    <input type="button" class="btn btn-danger" value="Refresh" onClick="location.href=location.href">
                    @csrf
                </form>

            </div>


            <div class="col-md-6">


                <h2>Receive Data Online</h2>


                <form class="form-inline" action="{{ route('receiveSyncData') }}" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Synchronize</label>
                        <div class="input-group">
                            <div class="input-group-addon">Synchronize</div>
                            <select class="form-control"  name="rec_item" style="width: 200px;" required>
                                <option value="">--Select--</option>
                                <option value="product">Product</option>
                                <option value="brand">Brand</option>
                                <option value="category">Category</option>
                                <option value="orders">Orders</option>
                                <option value="customers">Customers</option>
                                <option value="users">Users</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" name="rec_data" class="btn btn-success"> Synchronize </button>
                    <input type="button" class="btn btn-danger" value="Refresh" onClick="location.href=location.href">
                    @csrf
                </form>

            </div>

        </div>

    </div>







@endsection






