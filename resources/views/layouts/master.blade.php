<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title') </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ URL::to('lib/apple-icon.png') }}">
    <link rel="shortcut icon" href="{{ URL::to('lib/favicon.ico') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/selectFX/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/vendors/jqvmap/dist/jqvmap.min.css') }}">

    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.theme.min.css') }}" >


    <link rel="stylesheet" href="{{ URL::to('lib/assets/css/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>


<body>
@include('includes.header')


@yield('content')


<script src="{{ URL::to('lib/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/main.js') }}"></script>

<script src="{{ URL::to('lib/src/js/custom.js') }}"></script>


<script src="{{ URL::to('lib/vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/dashboard.js') }}"></script>
<script src="{{ URL::to('lib/assets/js/widgets.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ URL::to('lib/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>

<script src="{{ URL::to('lib/jquery-ui/jquery-ui.min.js') }}" ></script>


<script>

    (function ($) {
        "use strict";

        jQuery('#vmap').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#1de9b6',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#1de9b6', '#03a9f5'],
            normalizeFunction: 'polynomial'
        });

        $('#barcode_search')
            .on('submit', function (event) {
                event.preventDefault();
                //     let code      = $('#barcode_search input#bar_num').val();
                let route = $('#barcode_search').data('route');
                let form_data = $(this);
                $.ajax({
                    type: "POST",
                    url: route,
                    data: form_data.serialize()
                })
                    .done(function (data) {
                        console.log(data);
                        //    $('#myCart').append(html);
                        $("#myCart").html(data.cart);
                     //   $('#bar_num').focus();
                     //   $('#bar_num').val('');
                    });
                $('#bar_num').focus();
                $('#bar_num').val('');
            });


        $('#formSearch')
            .on('submit', function (event) {
                event.preventDefault();
                let name      = $('#formSearch input#txtProduct').val();
                let route = $('#formSearch').data('route');
                let form_data = $(this);
                $.ajax({
                    type: "POST",
                    url: route,
                    data: {
                        name: name,
                        "_token": "{{ csrf_token() }}"
                    }
                  //  data: form_data.serialize()
                })
                    .done(function (data) {
                        console.log(data);
                        $("#myCart").html(data.cart);
                    //    $('#txtProduct').focus();
                    //    $('#txtProduct').val('');
                    });
                $('#txtProduct').focus();
                $('#txtProduct').val('');
            });



        $('#myCart').on("click", '.inc_cart', function () {
            let id = $(this).data('id');
            modify_cart(id);
        });

        $('#myCart').on("click", '.dec_cart', function () {
            let id = $(this).data('id');
            $.ajax({
                method: 'POST',
                url: cartDecrease,
                data: {
                    id: id,
                    "_token": "{{ csrf_token() }}"
                }
            })
                .done(function (data) {
                    $("#myCart").html(data.cart);
                    $("#save_page").html(data.checkout_cart);
                })
        });

        $('#myCart').on("keyup", '.inp_numb', function () {
            var id = $(this).data('id');
            var unit = $(this).val();
            if (!unit) return;
            modify_cart(id, 'unit=' + unit + '&overwrite=true&', ".inp_numb[data-id=" + id + "]");
        });

        $('#myCart').on("click", '.del_cart', function () {
            let id = $(this).data('id');
            $.ajax({
                method: 'POST',
                url: cartDelete,
                data: {
                    id: id,
                    "_token": "{{ csrf_token() }}"
                }
            })
                .done(function (data) {
                    $("#myCart").html(data.cart);
                    $("#save_page").html(data.checkout_cart);
                })
        });


        function modify_cart(id, param, element) {
            $.ajax({
                method: 'POST',
                url: cartIncrease,
                data: {
                    id: id,
                    "_token": "{{ csrf_token() }}"
                }
            })
                .done(function (data) {
                    $("#cart_count").html(data.items_count);
                    $("#myCart").html(data.cart);
                    $("#save_page").html(data.checkout_cart);
                })
        }

        $('#customer_search')
            .on('submit', function($ev){
                $ev.preventDefault();
                let number = $('#customer_search input#phone_number').val();
                //  var $btn = $('#customer_search button[type="submit"]').button('loading');
                $.ajax({
                    method: 'POST',
                    url: searchCustomer,
                    data: {
                        number: number,
                        "_token": "{{ csrf_token() }}"
                    }
                }).done(function (data) {
                    $('#customerInfo').html(data.customer_details);
                    $('#phone_number').focus();
                    $('#phone_number').val('');
                })
            });

        $('#customer_search_name')
            .on('submit', function($ev){
                $ev.preventDefault();
                let name = $('#customer_search_name input#cus_name').val();
                //  var $btn = $('#customer_search button[type="submit"]').button('loading');
                $.ajax({
                    method: 'POST',
                    url: searchCustomerName,
                    data: {
                        name    : name,
                        "_token": "{{ csrf_token() }}"
                    }
                }).done(function (data) {
                    $('#customerInfo').html(data.customer_details);
                    $('#cus_name').focus();
                    $('#cus_name').val('');
                })
            });



        $("#dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "1900:currentYear.textContent"
        });

/*        function printPageArea(areaID){
            var printContent = document.getElementById(areaID);
            var WinPrint = window.open('', '', 'width=900,height=650');
            WinPrint.document.write(printContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }*/

        $('#printReceipt').click(function () {
           // printPageArea('#printArea')
           // document.querySelector('#printArea').print();
            window.print();
        });




        $('#customer_name').keyup(function(){
            let query = $(this).val();
            if(query !== '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.fetch') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#customerList').fadeIn();
                        $('#customerList').html(data);
                    }
                });
            }
        });

        $(document).on('click', 'li', function(){
            $('#customer_name').val($(this).text());
            $('#customerList').fadeOut();
        });

/*        $(document).ready(function () {
         //   $('#selectBrand').hide();
            $('#createBrand').hide();
            $('select#brand').change(function () {
                let value = $(this).val();
                switch (value) {
                    case 'createBrand':
                        $('#createBrand').show();
                        $('#selectBrand').hide();
                        break;
                    case 'selectBrand':
                        $('#selectBrand').show();
                        $('#createBrand').hide();
                        break;
                }
            })
        });*/


        let addBrand = $('.add_brand');
        let wrapper = $('.field_wrapper');
     //   let wrap = $('.brand_wrapper');
        let fieldHTML = '<div><input type="text" name="create_brand" placeholder="Enter Brand Name Here..." class="form-control"  required /><span style="font-size: large;" class="remove_button" title="Add field"><i class="icon-close"></i> </span></div>'; //New input field html
        let x = 1;

        $(addBrand).click(function() {
                $(wrapper).append(fieldHTML); //Add field html
        });

        $('.remove_button').click(function() {
            $(wrapper).remove();
          //  x--;
        });

        $('#txtProduct').keyup(function () {
            let query = $(this).val();
            if(query !== ''){
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('product.fetch') }}",
                    method: "POST",
                    data: {query:query, _token:_token},
                    success: function (data) {
                        $('#drugItems').fadeIn();
                        $('#drugItems').html(data);
                    }
                })
            }
        });

        $(document).on('click', 'li.drug', function () {
            $('#txtProduct').val($(this).text());
            $('#drugItems').fadeOut();
        });




    })(jQuery);


</script>

</body>

</html>
