@extends('layouts.master')

@section('title')
    BolaBlaque Beauty :: Register User
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> User Management   </h1>
                    @include('includes.message-block')
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="card">
            @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
            @endif
            <form action="{{ route('UserAccount.create') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong> Register User   </strong>
                </div>
                <div class="card-body card-block">


                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">First Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('first_name') }}" name="first_name" placeholder="Enter First Name..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">Last Name</label></div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('last_name') }}" name="last_name" placeholder="Enter Last Name..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Email Address</label></div>
                        <div class="col-12 col-md-9"><input type="email" value="{{ Request::old('email') }}" name="email" placeholder="Enter Email Address ..." class="form-control"></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="name" class=" form-control-label"> Role </label></div>
                        <div class="col-12 col-md-9">
                            <select class="form-control" name="role" required>
                                <option value="">--Select Role--</option>
                                <option value="User">User</option>
                                <option value="admin">admin</option>
                                <option value="Super admin">Super admin</option>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="card-footer">
                    @csrf {{-- {{ csrf_field() }}--}}
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                </div>
            </form>
        </div>
    </div>







@endsection



