@extends('layouts.master')

@section('title')
    Bola Blaque :: User Management
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>User Management</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    @if( Auth::user()->role != "User" )
                        <ol class="breadcrumb text-right">
                            <li class="active"><a type="button" class="btn btn-success" href="{{ route('UserAccount.create') }}">
                                    Create New User </a>
                            </li>
                        </ol>
                    @endif

                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                {{--<strong class="card-title">Stripped Table</strong>--}}

                <strong style="float: right" class="card-title">

               {{--     <form class="form-inline" id="basic-form" action="{{ route('customer.search') }}" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="customer_name"  placeholder="Search By Customer's Name"  required>
                            <button type="submit" class="btn btn-primary">Search</button>
                            <button type="reset" class="btn btn-success">Reset</button>
                            <button type="button" name="search" onClick="location.href=location.href" class="btn btn-danger">Refresh</button>
                        </div>

                        {{ csrf_field() }}
                    </form>--}}


                </strong>
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            @if( Auth::user()->role != "User" )
                            <th></th>
                            @endif
                      {{--      <th scope="col"></th>
                            <th scope="col"></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td> {{ $user->first_name  }} </td>
                                <td> {{ $user->last_name }} </td>
                                <td> {{ $user->email }} </td>
                                @if( Auth::user()->role != "User" )
                                    <td><a href="{{ route('UserAccount.delete' , ['id' => $user->id]) }}"> Delete </a></td>
                                @endif
                         {{--       <td><a href=""> &nbsp;Edit </a></td>--}}


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                       {{-- {{ $users->links() }}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>







@endsection



