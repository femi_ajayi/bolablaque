@extends('layouts.master')

@section('title')
    Bola Blaque :: User Profile
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> User Profile </h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">

                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3>Basic Details </h3>
            </div>
            <div class="card-body">




                                    <form id="basic-form" method="post" action="{{ route('UserAccount.updatePassword') }}">

                                            <div class="body">

                                                <div class="row clearfix">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" required>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row clearfix">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="username" value="{{ $user->email }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Role </label>
                                                            <input type="text" class="form-control" name="department" value="{{ $user->role }}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <hr />

                                            <div class="body">
                                                <h3> Password Settings </h3>
                                                <br/>
                                                @if(Session::has('success'))
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="alert alert-success">{{ Session::get('success') }}</p>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="row clearfix">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Current Password</label>
                                                            <input type="password" class="form-control" name="current_password" minlength="6" value="" required>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row clearfix">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>New Password</label>
                                                            <input type="password" class="form-control" name="new_password" minlength="6" value="" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row clearfix">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label> Confirm Password </label>
                                                            <input type="password" class="form-control" name="confirm_password" minlength="6" value="" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row clearfix">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            @csrf
                                                            <button type="submit" name="update_patient_record" class="btn btn-primary">Update Password</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">

                                                    </div>
                                                    <div class="col-sm-4">

                                                    </div>

                                                </div>

                                            </div>


                                    </form>




            </div>
        </div>
    </div>







@endsection



