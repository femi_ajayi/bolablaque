@extends('layouts.master')

@section('title')
    Bola Blaque :: Create Product
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Products </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
    </div>


    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('product.create') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong> Create New Product </strong>
                </div>
                <div class="card-body card-block">
                    {{--@include('includes.message-block')--}}
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    <div class="row form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">Product Name</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('name') }}" name="name"
                                                            placeholder="Enter Product Name..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div id="selectBrand" class="row form-group {{ $errors->has('brand_id') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label class=" form-control-label">Brand Name</label></div>
                        <div class="col-12 col-md-9 select_brand">
                            <select id="brand" class="form-control" name="brand_id" required>
                                <option value="">--Select Brand--</option>
                                {{--<option class="add_brand" value="createBrand">--Create Brand--</option>--}}
                                @foreach($brands as $brand)
                                    <option class="remove_button" value="{{$brand->id}}">{{$brand->name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9 field_wrapper">

                        </div>
                    </div>
                    <br/>





             {{--       <div class="row form-group {{ $errors->has('brand_id') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label class=" form-control-label">Brand </label></div>
                        <div class="col-12 col-md-9">
                            <select id="brand" class="form-control" name="brand" >
                                <option value=""></option>
                                <option value="selectBrand">--Select Brand--</option>
                                <option value="createBrand">--Create Brand--</option>
                            </select>
                        </div>
                    </div>--}}

       {{--             <div id="selectBrand" class="row form-group {{ $errors->has('brand_id') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label class=" form-control-label">Brand Name</label></div>
                        <div class="col-12 col-md-9">
                            <select id="brand" class="form-control" name="brand_id" required>
                                <option value="">--Select Brand--</option>
                                <option value="createBrand">--Create Brand--</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>

                    <div id="createBrand" class="row form-group {{ $errors->has('brand_id') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label class=" form-control-label">Brand Name</label></div>
                        <div class="col-12 col-md-9">
                            <div class="input-group" id="brandName">
                                --}}{{--<div class="input-group-addon">Switch To Select Brand</div>--}}{{--
                                <input type="text"  name="brand_name" placeholder="Enter Brand Name..." class="form-control" required>
                            </div>
                        </div>
                    </div>--}}

                    <div class="row form-group {{ $errors->has('categories') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label class=" form-control-label">Categories</label></div>
                        <div class="col-12 col-md-9">
                            <select class="form-control" name="categories">
                                <option value="">--Select Category--</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->name}}">{{$category->name}}</option>
                                @endForeach
                            </select>
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('cost_price') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="price" class=" form-control-label">Cost Price</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('cost_price') }}"
                                                            name="cost_price"
                                                            placeholder="Enter Product Cost Price..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('selling_price') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="price" class=" form-control-label">Selling Price</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('selling_price') }}"
                                                            name="selling_price"
                                                            placeholder="Enter Product Selling Price..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('quantity') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="quantity" class=" form-control-label">Stock</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('quantity') }}"
                                                            name="quantity"
                                                            placeholder="Enter Product Stock..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('barcode') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="barcode" class=" form-control-label">Barcode</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ Request::old('barcode') }}"
                                                            name="barcode"
                                                            placeholder="Enter Product Barcode..."
                                                            class="form-control">
                        </div>
                    </div>


                </div>
                <div class="card-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    {{--   <button type="reset" class="btn btn-danger btn-sm">
                           <i class="fa fa-ban"></i> Reset
                       </button>--}}
                </div>
            </form>
        </div>
    </div>







@endsection






