@extends('layouts.master')

@section('title')
    Bola Blaque :: Product Listing
@endsection


@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Products</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><a type="button" class="btn btn-success"
                                              href="{{ route('product.create') }}"> Create New Product </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">


                <div style="float: right" class="card-title">

                    <form class="form-inline" id="basic-form" action="{{ route('product.productName') }}" method="post">
                        <div class="form-group">
                            <input type="text"  class="form-control" placeholder="Search By Product Name"
                                   name="query" required>
                            <button type="submit" class="btn btn-primary">Search</button>
                            <a href="{{ route('product.index') }}" class="btn btn-warning">Refresh</a>
                            {{--<button type="button" name="search" onClick="location.href=location.href"  class="btn btn-warning">Refresh</button>--}}
                            @csrf
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('info'))
                    <div class="row">
                        <div class="col-md-12">
                            <p class="alert alert-info">{{ Session::get('info') }}</p>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>S/No.</th>
                            <th>Name</th>
                            <th>Category</th>
                           {{-- <th>Brand</th>--}}
                            <th>Cost Price</th>
                            <th>Selling Price</th>
                            <th>Stock</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td>{{ $product->name }}</td>
                                <td> {{ $product->categories }} </td>
                              {{--  <td> {{ $product->brand['name'] }} </td>--}}
                                <td>₦{{ number_format($product->cost_price) }}</td>
                                <td>₦{{ number_format($product->selling_price) }}</td>
                                <td> {{ $product->quantity }} </td>
                                <td><a  class='btn btn-warning' href="{{ route('product.edit' , ['id' => $product->id]) }}"  ><i class="fa fa-edit"></i></a></td>
                                <td> <a class="btn btn-danger" href="{{ route('product.delete', ['id' => $product->id]) }}"><i class="fa fa-trash"></i></a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                      {{--  {{ $products->links() }}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>







@endsection



