@extends('layouts.master')

@section('title')
    Bola Blaque :: Update Product
@endsection


@section('content')


    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> Update Products </h1>
                </div>
            </div>
        </div>
        @include('includes.message-block')
    </div>


    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('product.update') }}" method="post" class="form-horizontal">
                <div class="card-header">
                    <strong> Update Product </strong>
                </div>
                <div class="card-body card-block">
                    {{--@include('includes.message-block')--}}
                    @if(Session::has('info'))
                        <div class="row">
                            <div class="col-md-12">
                                <p class="alert alert-info">{{ Session::get('info') }}</p>
                            </div>
                        </div>
                 {{--   @endif--}}
                    @elseif(Session::has('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    <div class="row form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="name" class=" form-control-label">Product Name</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $product->name }}" name="name"
                                                            placeholder="Enter Product Name..."
                                                            class="form-control">
                        </div>
                    </div>


                    <div class="row form-group {{ $errors->has('cost_price') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="price" class=" form-control-label">Cost Price</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $product->cost_price }}"
                                                            name="cost_price"
                                                            placeholder="Enter Product Cost Price..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('selling_price') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="price" class=" form-control-label">Selling Price</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $product->selling_price }}"
                                                            name="selling_price"
                                                            placeholder="Enter Product Selling Price..."
                                                            class="form-control">
                        </div>
                    </div>

                    <div class="row form-group {{ $errors->has('quantity') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="quantity" class=" form-control-label">Stock</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $product->quantity }}"
                                                            name="quantity"
                                                            placeholder="Enter Product Stock..."
                                                            class="form-control">
                        </div>
                    </div>

{{--                    <div class="row form-group {{ $errors->has('barcode') ? 'has-error' : '' }} ">
                        <div class="col col-md-3"><label for="quantity" class=" form-control-label">Product Barcode</label>
                        </div>
                        <div class="col-12 col-md-9"><input type="text" value="{{ $product->barcode }}"
                                                            name="barcode"
                                                            placeholder="Enter Product Barcode..."
                                                            class="form-control">
                        </div>
                    </div>--}}




                </div>
                <div class="card-footer">
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Update
                    </button>
                    {{--   <button type="reset" class="btn btn-danger btn-sm">
                           <i class="fa fa-ban"></i> Reset
                       </button>--}}
                </div>
            </form>
        </div>
    </div>










@endsection



