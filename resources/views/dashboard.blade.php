@extends('layouts.master')

@section('title')
     BolaBlaque Beauty :: Main Dashboard
@endsection


@section('content')




    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Main Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
            {{--        <ol class="breadcrumb text-right">
                        <li class="active">Dashboard</li>
                    </ol>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">

        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> Welcome to Bolablaque Beauty Store.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   {{-- <span aria-hidden="true">&times;</span>--}}
                </button>
            </div>
        </div>




            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

                            </div>
                        </div>
                        <h4 class="mb-0">
                            <span class="count">100000</span>
                        </h4>
                        <p class="text-light"> Reports Available</p>

                        <div class="chart-wrapper px-0" style="height:50px;" >
                            <canvas id="widgetChart1"></canvas>
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton2" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                            </div>
                        </div>
                        <h4 class="mb-0">
                            <span class="count">100000</span>
                        </h4>
                        <p class="text-light"> Product Brand </p>

                        <div class="chart-wrapper px-0" style="height:50px;" >
                            <canvas id="widgetChart2"></canvas>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton3" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3">
                            </div>
                        </div>
                        <h4 class="mb-0">
                            <span class="count">100000</span>
                        </h4>
                        <p class="text-light"> Customer Details </p>
                    </div>
                    <div class="chart-wrapper px-0" style="height:50px;" >
                        <canvas id="widgetChart3"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-4">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                            <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton4" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton4">

                            </div>
                        </div>
                        <h4 class="mb-0">
                            <span class="count">100000</span>
                        </h4>
                        <p class="text-light"> Sales Reports </p>

                        <div class="chart-wrapper px-3" style="height:50px;" >
                            <canvas id="widgetChart4"></canvas>
                        </div>

                    </div>
                </div>
            </div>










    </div>
    </div>







@endsection
