<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('sync');
            $table->string('order_number');
            $table->string('salesperson');
            $table->integer('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('quantity');
            $table->string('amount_paid');
            $table->string('total_price');
            $table->text('payment_type');
            $table->text('orderItems');
            $table->string('returnStatus');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
