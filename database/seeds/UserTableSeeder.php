<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'sync'          => 'off',
            'first_name'    => 'admin',
            'last_name'     => 'user',
            'password'      =>  bcrypt('password1!'),
            'email'         => 'femi@yahoo.com',
            'role'          => "Super Admin",
            'registered_by' => "app"
        ]);
        $user->save();
    }
}
