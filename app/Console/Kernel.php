<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('command:sendproductonline')->everyMinute();
        $schedule->command('command:sendbrandonline')->everyMinute();
        $schedule->command('command:sendcategoryonline')->everyMinute();
        $schedule->command('command:sendordersonline')->everyMinute();
        $schedule->command('command:sendcustomeronline')->everyMinute();

        $schedule->command('command:receiveproductonline')->everyMinute();
        $schedule->command('command:receivebrandonline')->everyMinute();
        $schedule->command('command:receivecategoryonline')->everyMinute();
        $schedule->command('command:receivecustomeronline')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
