<?php

namespace App\Console\Commands;

use App\Brand;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ReceiveBrandOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:receivebrandonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive updated brand info from online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline"; // echo "yes";  exit;
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/brand/sendBrandToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $brand = Brand::where('brand_no', $value->brand_no)->first();
                if (!empty($brand)){
                    $brand->created_at      = $value->created_at;
                    $brand->updated_at      = $value->updated_at;
                    $brand->sync            = "on";
                    $brand->name            = $value->name;
                    $brand->save();
                }
                else {
                    $br                  = new Brand();
                    $br->created_at      = $value->created_at;
                    $br->updated_at      = $value->updated_at;
                    $br->sync            = "on";
                    $br->name            = $value->name;
                    $br->brand_no        = $value->brand_no;
                    $br->save();
                }
            }
            $is_conn = "Successfully sync brand";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
