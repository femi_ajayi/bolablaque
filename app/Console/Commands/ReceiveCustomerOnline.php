<?php

namespace App\Console\Commands;

use App\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ReceiveCustomerOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:receivecustomeronline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive updated customer info from online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/sendCustomerToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $customer = Customer::where('phone_no', $value->phone_no)->first();
                if (!empty($customer)){
                    $customer->created_at      = $value->created_at;
                    $customer->updated_at      = $value->updated_at;
                    $customer->sync            = "on";
                    $customer->full_name       = $value->full_name;
                    $customer->email           = $value->email;
                    $customer->phone_no        = $value->phone_no;
                    $customer->address         = $value->address;
                    $customer->birthdate       = $value->birthdate;
                    $customer->registered_by   = $value->registered_by;
                    $customer->save();
                }
                else {
                    $cu                  = new Customer();
                    $cu->created_at      = $value->created_at;
                    $cu->updated_at      = $value->updated_at;
                    $cu->sync            = "on";
                    $cu->full_name       = $value->full_name;
                    $cu->email           = $value->email;
                    $cu->phone_no        = $value->phone_no;
                    $cu->address         = $value->address;
                    $cu->birthdate       = $value->birthdate;
                    $cu->registered_by   = $value->registered_by;
                    $cu->save();
                }
            }
            $is_conn = "Successfully sync customers";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
