<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendOrdersOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendordersonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send updated orders info to online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $order = Order::where('sync', $sync)->get();
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/getOrderData', [$order]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $order = Order::where('order_number', $value->order_number)->first();
                $order->sync = "on";
                $order->save();
            }
            $is_conn = "Successfully sync orders";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
