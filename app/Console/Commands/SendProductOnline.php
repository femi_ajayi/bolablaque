<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendProductOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendproductonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send updated product info to online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $product = Product::where('sync', $sync)->get();
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/getProductData', [$product]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $prod = Product::where('barcode', $value->barcode)->first();
                $prod->sync = "on";
                $prod->save();
            }
            $is_conn = "Successfully sync products";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
      //  return $is_conn;
        echo "Testing things";
    }




}
