<?php

namespace App\Console\Commands;

use App\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendCustomerOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendcustomeronline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send updated customer info to online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $customer = Customer::where('sync', $sync)->get();
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/getCustomerData', [$customer]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $cu = Customer::where('phone_no', $value->phone_no)->first();
                $cu->sync = "on";
                $cu->save();
            }
            $is_conn = "Successfully sync customers";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
