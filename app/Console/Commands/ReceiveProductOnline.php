<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ReceiveProductOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:receiveproductonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive updated product info from online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/sendProductToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $prod = Product::where('barcode', $value->barcode)->first();
                if (!empty($prod)){
                    $prod->created_at      = $value->created_at;
                    $prod->updated_at      = $value->updated_at;
                    $prod->sync            = "on";
                    $prod->name            = $value->name;
                    $prod->categories      = $value->categories;
                    $prod->brand_id        = $value->brand_id;
                    $prod->cost_price      = $value->cost_price;
                    $prod->selling_price   = $value->selling_price;
                    $prod->quantity        = $value->quantity;
                    $prod->description     = $value->description;
                    $prod->barcode         = $value->barcode;
                    $prod->save();
                }
                else {
                    $product                  = new Product();
                    $product->created_at      = $value->created_at;
                    $product->updated_at      = $value->updated_at;
                    $product->sync            = "on";
                    $product->name            = $value->name;
                    $product->categories      = $value->categories;
                    $product->brand_id        = $value->brand_id;
                    $product->cost_price      = $value->cost_price;
                    $product->selling_price   = $value->selling_price;
                    $product->quantity        = $value->quantity;
                    $product->description     = $value->description;
                    $product->barcode         = $value->barcode;
                    $product->save();
                }
            }
            $is_conn = "Successfully sync product";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
