<?php

namespace App\Console\Commands;

use App\Category;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ReceiveCategoryOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:receivecategoryonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive updated category info from online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/category/sendCategoryToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $category = Category::where('category_no', $value->category_no)->first();
                if (!empty($category)){
                    $category->created_at      = $value->created_at;
                    $category->updated_at      = $value->updated_at;
                    $category->sync            = "on";
                    $category->name            = $value->name;
                    $category->category_no     = $value->category_no;
                    $category->save();
                }
                else {
                    $cat                  = new Category();
                    $cat->created_at      = $value->created_at;
                    $cat->updated_at      = $value->updated_at;
                    $cat->sync            = "on";
                    $cat->name            = $value->name;
                    $cat->category_no     = $value->category_no;
                    $cat->save();
                }
            }
            $is_conn = "Successfully sync category";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
