<?php

namespace App\Console\Commands;

use App\Brand;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendBrandOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendbrandonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send updated brand info to online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $brand = Brand::where('sync', $sync)->get(); //  echo $brand; dd("lkk");
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/brand/getBrandData', [$brand]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $br = Brand::where('brand_no', $value->brand_no)->first();
                $br->sync = "on";
                $br->save();
            }
            $is_conn = "Successfully sync brands";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return "$is_conn";
    }
}
