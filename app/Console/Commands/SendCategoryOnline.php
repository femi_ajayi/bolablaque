<?php

namespace App\Console\Commands;

use App\Category;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SendCategoryOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendcategoryonline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send updated category info to online server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $category = Category::where('sync', $sync)->get();
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/category/getCategoryData', [$category]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $cat = Category::where('category_no', $value->category_no)->first();
                $cat->sync = "on";
                $cat->save();
            }
            $is_conn = "Successfully sync category";
            fclose($connected);
        }else{
            $is_conn = "Connect to internet"; //action in connection failure
        }
        return $is_conn;
    }
}
