<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReceiptMail extends Mailable
{
    use Queueable, SerializesModels;

    public $carts;
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $order, $carts)
    {
        $this->carts    = $carts;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no_reply@bolablaquebeauty.com')
         //   ->view('orders.mailDetails', ['order_id' => $this->order ]);
            ->view('orders.mailDetails', ['carts' => $this->carts, 'order' => $this->order ]);
       // return $this->view('view.name');
    }
}
