<?php

namespace App;



use Illuminate\Support\Facades\Session;

class UserCart
{

    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart)
        {
            $this->items      = $oldCart->items;
            $this->totalQty   = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $id)
    {
        $storedItem = [ 'qty'           => 0,
                        'id'            => $item->id,
                        'name'          => $item->name,
                        'selling_price' => $item->selling_price,
                        'total_price'   => $item->total_price,
                        'item'          => $item
        ];
        if ($this->items) {
            if (array_key_exists($id, $this->items)){
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['id']   = $item->id;
        $storedItem['name'] = $item->name;
        $storedItem['selling_price'] = $item->selling_price;
        $storedItem['total_price'] = $item->selling_price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $item->selling_price;
    }

    public function reduceByOne($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['total_price'] = $this->items[$id]['qty'] * $this->items[$id]['selling_price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['selling_price'];

        if($this->items[$id]['qty'] <= 0){
            unset($this->items[$id]);
        }
    }

    public function removeItem($id)
    {

        $this->totalQty   -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['total_price'];
        unset($this->items[$id]);

    }


    public static function render_cart()
    {
        $cart = Session::get('cart');

        if (empty($cart->items))
            return "<h4>No Product was selected!</h4>";

        $text = '<form action="'.route('ordersSave').'" method="post" ><div class="user-cart" ><table class="table table-bordered table-condensed table-hover">
                                <thead>
                           <tr>
                            <th>Item(s)</th>
                            <th>Quantity</th>
                            <th> Unit Price</th>
                            <th>Sub-Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>';

        foreach ($cart->items as $item)
            $text .= static::get_single_cart_item($item);
      //  $total = static::total_price();
        $total = $cart->totalPrice;
        $text .= "<tr>
                <td colspan='4' class='text-left'><strong>Total  Price : </strong></td>
                <td colspan='5' class='text-left'><strong>₦$total</strong></td>
                </tr>";

        $text .= '</tbody></table>
                 <div>
                <!-- <form action="save_cart" method="post" >-->
                    <input type="hidden" name="_token" value="'.Session::token().'">
                <!--    <button  type="submit" name="park" class="btn btn-warning">Park Sales</button> -->
                    <button  type="submit" name="continue" class="btn btn-danger">Submit</button>
                </form>
                </div>
                </div>';

        return $text;

    }

    protected static function get_single_cart_item($product)
    {

        return "<tr>
                <td> $product[name]  </td>
                <td><input type='number' class='form-control inp_numb' min='1' value='$product[qty]' style='width:70px;' readonly ></td>
                <td>₦$product[selling_price]</td>
                <td>₦$product[total_price]</td>
                <td><span data-id=$product[id]   class='ti-plus inc_cart'></span></td>
                <td><span data-id=$product[id]  class='ti-minus dec_cart'></span></td>
                <td><span data-id=$product[id]  class='ti-close del_cart'></span></td>

             </tr>";
    }


    public static function customer_details()
    {
        $customer = Session::get('customer');

        $phone_number = Session::get('phone_number');

        if (empty($customer))  {

/*            $text = "<h4>No customer has this phone number! Click <a id='show_hide' href='#'>here</a> </h4>";
            return $text;*/

          //  return "<h4>No customer has this phone number! Click <a id='show_hide' href='#'>here</a> </h4>";
            $text="<table class='table table-bordered'>
                                    <thead>
                                        <tr>
                                            <th>Customer Name</th>
                                            <td><input class='form-control' name='name' ></td>
                                        </tr>
                                    </thead>";
            $text .=                             "<tbody>
                                        <tr>
                                             <th> Email Address </th>
                                            <td ><input class='form-control' name='email' ></td>
                                        </tr>";

            $text .=                    "<tbody>
                                        <tr>
                                             <th >Phone Number</th>
                                            <td ><input class='form-control' value='$phone_number' name='phone_no' ></td>
                                        </tr>";

            $text .=                             "<tbody>
                                        <tr>
                                             <th >Address</th>
                                            <td ><input class='form-control' name='address'></td>
                                        </tr>";

            $text .=                             "<tbody>
                                        <tr>
                                             <th >Birthdate </th>
                                            <td ><input class='form-control' type='date' name='birthdate'></td>
                                        </tr>
                                    </tbody>
                                </table>";
            return $text;
        } else {
            $text = "<input name='customer_id' value='$customer->id' hidden />";
            $text.="<table class='table table-bordered'>
                                    <thead>
                                        <tr>
                                            <th>Customer Name</th>
                                            <td>$customer->full_name</td>

                                        </tr>
                                    </thead>";

            $text .=                             "<tbody>
                                        <tr>
                                             <th >Phone Number</th>
                                            <td >$customer->phone_no</td>
                                        </tr>";

            $text .=                             "<tbody>
                                        <tr>
                                             <th >Address </th>
                                            <td >$customer->address</td>
                                        </tr>
                                    </tbody>
                                </table>";
            return $text;
        }
    }

    public static function customer_name_details()
    {
        $customers = Session::get('customers');
            $text = "<table class='table table-bordered'>
                                 <thead>
                                   <tr>
                                        <th>Customer Name</th>
                                          <th>Phone No.</th>
                                            <th>Select</th>

                                        </tr>
                                   </thead>";
            foreach ($customers as $cus){
                $text .=                     "<tbody>
                                        <tr>
                                             <th> $cus->full_name </th>
                                            <td > $cus->phone_no </td>
                                            <td> <input type='checkbox' name='customer_id' value='$cus->id' ></td>
                                        </tr></tbody>";
            }
            $text.="</table>";

        return $text;
    }


  /*  public static function order_details()
    {
        $orders = Session::get('orders');

        if (empty($orders))  {
            $text = "<h4>No sales for this date range </h4>";
            return $text;
        } else {
            $text ="<div class='table-responsive'><table class='table table-striped'>
                                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Served By</th>
                            <th>Date </th>
                            <th></th>
                            <th></th>

                        </tr>
                        </thead><tbody>";
            foreach($orders as $order)
                $text .= "
                           <tr>
                                <td>{{  }}</td>
                                <td> {{ (isset($order->customer_id)) ? $order->customer['full_name'] : 'Generic Customer' }}  </td>
                                <td> {{ $order->salesperson }}  </td>
                                <td> {{  date('d/m/Y h:i a', strtotime($order->created_at)) }} </td>
                                <td><a href=''> &nbsp; View Details </a></td>
                                <td><a href=''> &nbsp; Return Items </a></td>

                            </tr>"

             endforeach
         $text.= "    </tbody>
                    </table>
                </div>";
            return $text;
        }
    }*/









}
