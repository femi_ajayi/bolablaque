<?php


use App\Customer;

function getAge($birthDate){
    $birthDate = date('m/d/Y', strtotime($birthDate));
    //   $birthDate = "1/27/1986";
    //explode the date to get month, day and year
    $birthDate = explode("/", $birthDate);
    //get age from date or birthdate
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));
    return $age;
    //  echo "Age is:" . $age;
}




function getBirthdays() {
    $date = now();
    return Customer::whereMonth('birthdate', '=', $date->month)
        ->whereDay('birthdate', '=', $date->day)
        ->count();
}


