<?php

namespace App;


class ServiceResult
{
    public $success = false;
    public $message;
    public $items_count;
    public $cart;
    public $customer_details;
    public $checkout_cart;
    public $order_details;

}
