<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    public function viewCategoryIndex()
    {
      //  $categories = Category::all();
        $categories = Category::orderBy('name')->paginate(20);
        $index = 1;
        return view('category.index', ['categories' => $categories, 'index' => $index]);
    }

    public function viewCategoryByName(Request $request){
        $index = 1;
        $query  = $request['query'];
        $categories  = DB::table('categories')
            ->where('name', 'LIKE', "%{$query}%")
            ->get();
        return view('category.categoryName', ['categories' => $categories, 'index' => $index]);
    }

    public function getCategoryCreate()
    {
        return view('category.create');
    }

    public function postCategoryCreate(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|unique:categories',
             'category_no'  => 'unique:categories'
        ]);

        $category               = new Category();
        $category->sync         = "off";
        $category->name         = $request['name'];
        $category->category_no  = rand(100000,999999);
        if ($category->save()){
            return redirect()->route('category.index')->with(['info' => 'Category Successfully created']);
        }
    }


    public function getCategoryEdit($id)
    {
        $category = Category::find($id);
        return view('category.edit', ['category' => $category]);
    }


    public function postCategoryUpdate(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required',
        ]);

        $category = Category::find($request->input('id'));
        $category->sync            = "off";
        $category->name            = $request->input('name');
        if ($category->save()){
            return redirect()->route('category.index')->with('info', 'Category Updated!' );
        }
        return redirect()->back()->with('fail', 'Could not update category!');

    }

    public function getCategoryDelete($id)
    {
        $category = Category::find($id);
        return view('category.delete', ['category' => $category]);
    }

    public function postCategoryDelete(Request $request)
    {
        $category = Category::find($request->input('id'));
        if ($category->delete()){
            return redirect()->route('category.index')->with(['success' => 'Successfully deleted']);
        }
        return redirect()->back()->with('fail', 'Could not delete category!');
    }

    public function addCategoryNumber(){
        $categories = Category::where('category_no', 0)->get();
        foreach ($categories as $category) {
            $qrcontent = rand(100000, 999999);
            $category->category_no = $qrcontent;
            $category->save();
        }
    }





    public  function sendCategoryOnline() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $category = Category::where('sync', $sync)->get(); //  echo $brand; dd("lkk");
            //   $response = Http::post('http://localhost/bolablaque_on/public/api/api/v1/brand/getBrandData',  [$brand] );
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/category/getCategoryData', [$category]);
            //  echo $response;  dd("pi");
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $cat = Category::where('category_no', $value->category_no)->first();
                $cat->sync = "on";
                $cat->save();
            }
            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    public  function receiveCategory() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/category/sendCategoryToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $category = Category::where('category_no', $value->category_no)->first();
                if (!empty($category)){
                    $category->created_at      = $value->created_at;
                    $category->updated_at      = $value->updated_at;
                    $category->sync            = "on";
                    $category->name            = $value->name;
                    $category->category_no     = $value->category_no;
                    $category->save();
                }
                else {
                    $cat                  = new Category();
                    $cat->created_at      = $value->created_at;
                    $cat->updated_at      = $value->updated_at;
                    $cat->sync            = "on";
                    $cat->name            = $value->name;
                    $cat->category_no     = $value->category_no;
                    $cat->save();
                }

            }

            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;

    }



// Online Methods-----------------------------------------------------------
    public function getCategoryData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        //  dd(print_r($decoded));
        foreach ($decoded[0] as $item){
            // echo gettype($item);
            $category = Category::where('category_no', $item->category_no)->first();
            if (!empty($category)){
                $category->created_at      = $item->created_at;
                $category->updated_at      = $item->updated_at;
                $category->sync            = "on";
                $category->name            = $item->name;
                $category->category_no     = $item->category_no;
                $category->save();
            }
            else {
                $cat                  = new Category();
                $cat->created_at      = $item->created_at;
                $cat->updated_at      = $item->updated_at;
                $cat->sync            = "on";
                $cat->name            = $item->name;
                $cat->category_no     = $item->category_no;
                $cat->save();
            }
        }
        return $decoded;
    }

    public function sendCategoryToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $category = Category::where('sync', $sync)->get();
        foreach ($category as $c){
            $c->sync =  "on";
            $c->save();
        }
        return $category;
    }






}
