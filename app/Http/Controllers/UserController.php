<?php

namespace App\Http\Controllers;


use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function getDashboard()
    {
        return view('dashboard');
    }

    public function postLogIn(Request $request)
    {
        $this->validate($request, [
            'email'      => 'required',
            'password'   => 'required|min:4',
        ]);
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password'] ])){
            // dd("check");
            return redirect()->route('dashboard');
        }
        return redirect()->back()->with('fail', 'Authentication failed');
    }

    public function getNewUser()
    {
        return view('UserAccount.create');
    }

    public function postNewUser(Request $request)
    {
        //  dd("djd");
        $this->validate($request, [
            'email'      => 'required|unique:users|max:80',
            //   'password'      => 'required|min:4',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'role'          => 'required'
            //  'registered_by' => 'required'
        ]);


        $user   = new User([
            'sync'          => "off",
            'email'         => $request->input('email'),
            'password'     =>  bcrypt("123456"),
            //  'password'      => bcrypt($request->input('password')),
            'first_name'    => $request->input('first_name'),
            'last_name'     => $request->input('last_name'),
            'role'          => $request->input('role'),
            'registered_by' => Auth::user()->first_name . " " . Auth::user()->last_name

        ]);
        if ($user->save()){
            //  Auth::login($user);
            return redirect()->route('UserAccount.index')->with('info', 'User created successfully!' );
        }
        return redirect()->back()->with('fail', 'Authentication failed');

    }

    public function getLogout()
    {
        Auth::guard('web')->logout();
        //  Auth::logout();
        return redirect()->route('home');
    }

    public function viewAllUsers()
    {
        $index = 1;
        $users = User::all();
        return view('UserAccount.index', ['users' => $users, 'index' => $index]);
    }

    public function getProfileDetails($id)
    {
        $user = User::find($id);
        return view('UserAccount.profile', ['user' => $user]);
    }

    public function DeleteUser($id)
    {
        $user = User::find($id);
        if($user->delete()){
            return redirect()->route('UserAccount.index');
        }
        return redirect()->back()->with('fail', 'Unable to delete user!');
    }

    public function UpdatePassword(Request $request)
    {
        $this->validate($request, [
            'current_password'  => ['required', new MatchOldPassword()],
            'new_password'      => 'required|min:6',
            'confirm_password'  => 'required|same:new_password',
        ]);

        User::find(auth()->user()->id)->update(
            ['password' => Hash::make($request->new_password)]
        );
        return redirect()->back()->with('success', 'Password has been changed');
    }






    public  function sendUserOnline() {
        $sync = "off";
        $user = User::where('sync', $sync)->get();
        $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/user/getUserData', [$user]);
        $decoded = json_decode($response);
        foreach ($decoded[0] as $value){
            $usR = User::where('email', $value->email)->first();
            $usR->sync = "on";
            $usR->save();
        }
    }

    public  function receiveUser() {
        $data = "offline";
        $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/user/sendUserToLocal', [$data]);
        $decoded = json_decode($response);
        foreach ($decoded as $value){
            $user = User::where('email', $value->email)->first();
            if (!empty($user)){
                $user->created_at      = $value->created_at;
                $user->updated_at      = $value->updated_at;
                $user->sync            = "on";
                $user->email           = $value->email;
                $user->password        = $value->password;
                $user->first_name      = $value->first_name;
                $user->last_name       = $value->last_name;
                $user->role            = $value->role;
                $user->registered_by   = $value->registered_by;
                $user->save();
            }
            else {
                $usR                  = new User();
                $usR->created_at      = $value->created_at;
                $usR->updated_at      = $value->updated_at;
                $usR->sync            = "on";
                $usR->email           = $value->email;
                $usR->password        = $value->password;
                $usR->first_name      = $value->first_name;
                $usR->last_name       = $value->last_name;
                $usR->role            = $value->role;
                $usR->registered_by   = $value->registered_by;
                $usR->save();
            }

        }
    }


// Online Methods-----------------------------------------------------------
    public function getUserData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        foreach ($decoded[0] as $item){
            $user = User::where('email', $item->email)->first();
            if (!empty($user)){
                $user->created_at      = $item->created_at;
                $user->updated_at      = $item->updated_at;
                $user->sync            = "on";
                $user->email           = $item->email;
                $user->password        = $item->password;
                $user->first_name      = $item->first_name;
                $user->last_name       = $item->last_name;
                $user->role            = $item->role;
                $user->registered_by   = $item->registered_by;
                $user->save();
            }
            else {
                $usR                  = new User();
                $usR->created_at      = $item->created_at;
                $usR->updated_at      = $item->updated_at;
                $usR->sync            = "on";
                $usR->email           = $item->email;
                $usR->password        = $item->password;
                $usR->first_name      = $item->first_name;
                $usR->last_name       = $item->last_name;
                $usR->role            = $item->role;
                $usR->registered_by   = $item->registered_by;
                $usR->save();
            }
        }
        return $decoded;
    }

    public function sendUserToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $user = User::where('sync', $sync)->get();
        foreach ($user as $u){
            $u->sync =  "on";
            $u->save();
        }
        return $user;
    }



}
