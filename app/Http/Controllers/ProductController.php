<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Customer;
use App\Mail\ReceiptMail;
use App\Order;
use App\Parking;
use App\Product;
use App\ServiceResult;
use App\User;
use App\UserCart;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use stdClass;


class ProductController extends Controller
{
    public function viewProductIndex()
    {
        $products = Product::orderBy('created_at')->paginate(100);
        $index = 1;
        return view('product.index', ['products' => $products, 'index' => $index]);
        //    return view('product.index');
    }

    public function viewProductByName(Request $request){
        $index = 1;
        $query  = $request['query'];
        $products  = DB::table('products')
            ->where('name', 'LIKE', "%{$query}%")
            ->get();
        return view('product.productName', ['products' => $products, 'index' => $index]);
    }

    public function getProductCreate()
    {
        $brands = Brand::all();
        $categories = Category::all();
        return view('product.create', ['brands' => $brands, 'categories' => $categories]);

    }

    public function postProductCreate(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|unique:products',
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required',
            /*'brand_id'      => 'required',*/
            'barcode'       => 'required|unique:products'

        ]);

  //      echo $request['create_brand'] . "<br/>";
  //      echo "yes";  exit;

        $product                  = new Product();
        $product->sync            = "off";
        $product->name            = $request['name'];
        $product->cost_price      = $request['cost_price'];
        $product->selling_price   = $request['selling_price'];
        $product->quantity        = $request['quantity'];
        $product->description     =   "";
        $product->brand_id        = $request['brand_id'];
        $product->categories      = $request['categories'];
        $product->barcode         = $request['barcode'];

        if ($product->save()){
            return redirect()->route('product.index')->with(['info' => 'Product Successfully created']);
        }
        return redirect()->back()->with('fail', 'Unable to create product!');
    }


    public function getProductEdit($id)
    {
        $product = Product::find($id);
        return view('product.edit', ['product' => $product]);
    }

    public function postProductUpdate(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required',
            'cost_price'      => 'required',
            'selling_price'   => 'required',
            'quantity'        => 'required',
         //   'barcode'       => 'required'
         //   'barcode'       => 'required|unique:products'
        ]);

    //    echo $request->input('id'); exit;

        $product = Product::find($request->input('id'));
        $product->sync            = "off";
        $product->name            = $request->input('name');
        $product->cost_price      = $request->input('cost_price');
        $product->selling_price   = $request->input('selling_price');
        $product->quantity        = $request->input('quantity');
        $product->description     = "";
      //  $product->barcode         = $request['barcode'];
        if ($product->save()){
            return redirect()->route('product.edit', ['id' => $product->id])->with('info', 'Product Updated!' );
         //   return redirect()->route('product.index')->with('info', 'Product Updated!' );
        }
        return redirect()->back()->with('fail', 'Could not update product!');

    }

    public function getProductDelete($id)
    {
        $product = Product::find($id);
        return view('product.delete', ['product' => $product]);
    }

    public function postProductDelete(Request $request)
    {
        $product = Product::find($request->input('id'));
        if ($product->delete()){
            return redirect()->route('product.index')->with(['success' => 'Successfully deleted']);
        }
        return redirect()->back()->with('fail', 'Could not delete product!');
    }


    public function getOrdersCreate(){
        Session::forget('cart');
        return View('orders.create');
    }

    public function getOrdersCreateName(){
        Session::forget('cart');
        return View('orders.createName');     
    }


    public function getAddToCart(Request $request)
    {
        $barcode = $request['barcode'];
        $product = Product::where('barcode', $barcode)
            ->where('quantity', '!=', 0)
            ->first();

        $cart =  Session::get('cart');
        foreach ($cart->items as $c){
            if ($product->name == $c['name']){
                if($product->quantity <= $c['qty']){
                    dd("nothing");
                }
            }
        }

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new UserCart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);  // OR Session::put()->('cart', $cart);
        //  dd($request->session()->get('cart'));

        $result = new ServiceResult();

        $result->message = "Successfully added a product to the cart";
        $result->success = true;
        $result->cart    =  UserCart::render_cart();   //  Session::get('cart');  // UserCart::render_cart();
        $result->items_count = Session::get('cart')->totalQty;
        $result->checkout_cart =  "0";   // UserCart::checkout_page();

        return response()->json($result);

    }

    public function getAddToCartNyName(Request $request)
    {
        $product="";
        $name = $request['name'];
        $barcode = $request['barcode'];

        if (isset($name)){
            $product = Product::where('name', $name)
                ->where('quantity', '!=', 0)
                ->first();
        }
        else if (isset($barcode)){
            $product = Product::where('barcode', $barcode)
                ->where('quantity', '!=', 0)
                ->first();
        }


        if (Session::get('cart')){
            $cart =  Session::get('cart');
            foreach ($cart->items as $c){
                if ($product->name == $c['name']){
                    if($product->quantity <= $c['qty']){
                        dd("nothing");
                    }
                }
            }
        }





        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new UserCart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);  // OR Session::put()->('cart', $cart);
        //  dd($request->session()->get('cart'));

        $result = new ServiceResult();

        $result->message = "Successfully added a product to the cart";
        $result->success = true;
        $result->cart    =  UserCart::render_cart();   //  Session::get('cart');  // UserCart::render_cart();
        $result->items_count = Session::get('cart')->totalQty;
        $result->checkout_cart =  "0";   // UserCart::checkout_page();

        return response()->json($result);

    }


    public function increaseCartOrders(Request $request) {
        $id = $request['id'];
        $product = Product::find($id);

        $cart =  Session::get('cart');

        foreach ($cart->items as $c){
            if ($product->name == $c['name']){
                if($product->quantity <= $c['qty']){
                    dd("nothing");
                }
            }
        }


        $oldCart = Session::has('cart') ? Session::get('cart') : null;


        $cart = new UserCart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);  // OR Session::put()->('cart', $cart);
        //  dd($request->session()->get('cart'));

        $result = new ServiceResult();

        $result->message = "Successfully added a product to the cart";
        $result->success = true;
        $result->cart    =  UserCart::render_cart();   //  Session::get('cart');  // UserCart::render_cart();
        $result->items_count = Session::get('cart')->totalQty;
        $result->checkout_cart =  "0";   // UserCart::checkout_page();

        return response()->json($result);
    }

    public function decreaseCartOrders(Request $request) {
        $id = $request['id'];
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new UserCart($oldCart);

        $cart->reduceByOne( $id);

        if (count($cart->items) > 0){
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        $result = new ServiceResult();

        $result->message = "Successfully added a product to the cart";
        $result->success = true;
        $result->cart    =  UserCart::render_cart();   //  Session::get('cart');  // UserCart::render_cart();
     //   $result->items_count = Session::get('cart')->totalQty;
        $result->checkout_cart =  "0";   // UserCart::checkout_page();

        return response()->json($result);
    }

    public function deleteCartOrders(Request $request) {
        $id = $request['id'];
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new UserCart($oldCart);
        $cart->removeItem( $id);

        if (count($cart->items) > 0){
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        $result          = new ServiceResult();
        $result->message = "Successfully added a product to the cart";
        $result->success = true;
        $result->cart    =  UserCart::render_cart();
      //  $result->items_count = Session::get('cart')->totalQty;
        $result->checkout_cart =  "0";   // UserCart::checkout_page();

        return response()->json($result);
    }

    public function finalCartPage() {

        if (isset($_POST['park'])) {
            if (Session::has('cart')){
              //  Session::put('cart', 'park');

                $park =  Session::get('cart');

             //   dd($park); // exit;

                $parking              = new Parking();
                $parking->items      = json_encode($park->items);
                $parking->totalQty   = $park->totalQty;
                $parking->totalPrice = $park->totalPrice;
                if ($parking->save()){
                    Session::forget('cart');
                    return "done";
                    // return view('orders.checkout');
                }
            }

        }
        if (isset($_POST['continue'])) {
            if (!Session::has('cart')){
                return view('orders.checkout');
            }
            $oldCart = Session::get('cart');
            $cart = new UserCart($oldCart);
            $total = $cart->totalPrice;

            return view('orders.checkout', ['total' => $total, 'cart' => $cart->items]);
        }



    }

    public function ViewParked() {
        $index = 1;
        $parked = Parking::all();
     //   $items = json_decode($parked->items);
        return view('parking.index', ['parkings' => $parked, 'index' => $index]);
    }




    public function postCheckoutPage(Request $request) {
/*        $name     = $request['name'];
        $phone_no = $request['phone_no'];
        $address  = $request['address'];
        $email    = $request['email'];*/


        if(isset($request['name']) and isset($request['phone_no'])){
            $customer            = new Customer();
            $customer->sync      = "off";
            $customer->full_name = $request['name'];
            $customer->email     = $request['email'];
            $customer->phone_no  = $request['phone_no'];
            $customer->address  = $request['address'];
            if (empty($request['birthdate'])){
                $customer->birthdate = $request['birthdate'];
            } else {
                $customer->birthdate = date("Y-m-d", strtotime($request['birthdate']));
            }
            $customer->registered_by = Auth::user()->first_name . " " . Auth::user()->last_name;
            $customer->save();
        }

        $customer_id   = $request['customer_id'];
        $discount      = $request['discount'];
        $payment       = $request['payment'];

        $errorProduct = "";
        $errorMessage = "";
        if (Session::has('cart')){
            $cart =  Session::get('cart');
            foreach ($cart->items as $item){
                $product = Product::where('id', $item['id'])->first();
                if ($product->quantity >= $item['qty']){
                  //  $product->quantity -= $item['qty'];
                  //  $product->sync      = "off";
                  //  $product->save();
                } else {
                    $errorProduct .= "$product->name quantity not sufficient," ;
                }
            }
            $errorMessage .= $errorProduct;
            if (empty($errorMessage)) {
                foreach ($cart->items as $item) {
                    $product = Product::where('id', $item['id'])->first();
                    if ($product->quantity >= $item['qty']){
                          $product->quantity -= $item['qty'];
                          $product->sync      = "off";
                          $product->save();
                        }
                  }
                $order_number        = static::get_order_number();
                $amount_paid         = $cart->totalPrice - $discount;

                $order               = new Order();
                $order->sync         = "off";
                $order->order_number = $order_number;
                $order->salesperson  = Auth::user()->first_name . " " . Auth::user()->last_name;
                if (isset($customer->id)){
                    $order->customer_id = $customer->id;
                    $order->customer_name = $customer->full_name;
                } else {
                    if (empty($customer_id)){
                        $order->customer_id   = NULL;
                        $order->customer_name = "";
                    }else {
                        $cus = Customer::find($customer_id);
                        $order->customer_id   = $customer_id;
                        $order->customer_name = $cus->full_name;
                    }

                }
                $order->quantity     = $cart->totalQty;
                $order->amount_paid  = $amount_paid;
                $order->total_price  = $cart->totalPrice;
                $order->payment_type = $payment;
                $order->orderItems   = serialize($cart);
                $order->returnStatus = "NO";
                if ($order->save()){
                    Session::forget('cart');
                    return redirect()->route('orders.re-print', ['id' => $order->id])->with(['info' => 'Orders Successfully created']);
                  //  return redirect()->route('orders.print', ['id' => $order->id])->with(['info' => 'Orders Successfully created']);
                }
            } return redirect()->route('orders.createName')->with('fail', $errorMessage);
            // return redirect()->back()->with('fail', $errorMessage);
        }
    }

    public function postReturn(Request $request){
        $order_id  = $request['order_id'];
        $order = Order::find($order_id);
        $t = unserialize($order->orderItems);
        $order->sync         = "off";
        $order->returnStatus = "YES";
       // if ($order->delete()){
        if ($order->save()){
            foreach ($t->items as $item){
                $product = Product::find($item['id']);
                $product->quantity += $item['qty'];
                $product->sync      = "off";
                $product->save();
            }
        }
        return redirect()->route('orders.sales')->with(['info' => 'Return successfully done!']);
    }

    public function getSyncData() {
        return view("sync.index");
    }

    public function postSyncData(Request $request){
        $sync_item  = $request['sync_item'];
        switch ($sync_item) {
                case 'product':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $sync = "off";
                    $product = Product::where('sync', $sync)->get();
                    //  $response = Http::post('http://localhost/bolablaque_on/public/api/api/v1/product/getProductData',  [$product] );
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/getProductData', [$product]);
                    $decoded = json_decode($response);
                    //  echo gettype($decoded[0]);  echo "<br/>";
                    foreach ($decoded[0] as $value){
                        //   echo $value->barcode . "<br/>";
                        $prod = Product::where('barcode', $value->barcode)->first();
                        $prod->sync = "on";
                        $prod->save();
                    }
                    $is_conn = "Successfully sync products";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
        break;
                case 'orders':
                    $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                    if ($connected){
                        $sync = "off";
                        $order = Order::where('sync', $sync)->get();
                        $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/getOrderData', [$order]);
                        $decoded = json_decode($response); // echo $response; exit;
                        foreach ($decoded[0] as $value){
                            //   echo $value->barcode . "<br/>";
                            $order = Order::where('order_number', $value->order_number)->first();
                            $order->sync = "on";
                            $order->save();
                        }
                        $is_conn = "Successfully sync orders";
                        fclose($connected);
                    }else{
                        $is_conn = "Connect to internet"; //action in connection failure
                    }
                    return $is_conn;
        break;
                case 'brand':
                    $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                    if ($connected){
                        $sync = "off";
                        $brand = Brand::where('sync', $sync)->get(); //  echo $brand; dd("lkk");
                        $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/brand/getBrandData', [$brand]);
                        $decoded = json_decode($response);
                        foreach ($decoded[0] as $value){
                            $br = Brand::where('brand_no', $value->brand_no)->first();
                            $br->sync = "on";
                            $br->save();
                        }
                        $is_conn = "Successfully sync brands";
                        fclose($connected);
                    }else{
                        $is_conn = "Connect to internet"; //action in connection failure
                    }
                    return "$is_conn";
        break;
            case 'category':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $sync = "off";
                    $category = Category::where('sync', $sync)->get(); //  echo $brand; dd("lkk");
                    //   $response = Http::post('http://localhost/bolablaque_on/public/api/api/v1/brand/getBrandData',  [$brand] );
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/category/getCategoryData', [$category]);
                    //  echo $response;  dd("pi");
                    $decoded = json_decode($response);
                    foreach ($decoded[0] as $value){
                        $cat = Category::where('category_no', $value->category_no)->first();
                        $cat->sync = "on";
                        $cat->save();
                    }
                    $is_conn = "Successfully sync category";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
            case 'customers':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $sync = "off";
                    $customer = Customer::where('sync', $sync)->get();  // dd($customer);  exit;
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/getCustomerData', [$customer]);
                    $decoded = json_decode($response);  // dd(gettype($decoded));  exit;
                    foreach ($decoded[0] as $value){
                        $cu = Customer::where('phone_no', $value->phone_no)->first();
                        $cu->sync = "on";
                        $cu->save();
                    }
                    $is_conn = "Successfully sync customers";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
        break;
            case 'users':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $sync = "off";
                    $usR = User::where('sync', $sync)->get();
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/user/getUserData', [$usR]);
                    $decoded = json_decode($response); // echo $response; exit;
                    foreach ($decoded[0] as $value){
                        $user = User::where('email', $value->email)->first();
                        $user->sync = "on";
                        $user->save();
                    }
                    $is_conn = "Successfully sync users";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            default:
                    return "Unknown error";
            }

    }

    public function receiveSyncData(Request $request){
        $rec_item  = $request['rec_item'];
        switch ($rec_item) {
            case 'product':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $data = "offline";
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/sendProductToLocal', [$data]);
                    $decoded = json_decode($response);
                    foreach ($decoded as $value){
                        $prod = Product::where('barcode', $value->barcode)->first();
                        if (!empty($prod)){
                            $prod->created_at      = $value->created_at;
                            $prod->updated_at      = $value->updated_at;
                            $prod->sync            = "on";
                            $prod->name            = $value->name;
                            $prod->categories      = $value->categories;
                            $prod->brand_id        = $value->brand_id;
                            $prod->cost_price      = $value->cost_price;
                            $prod->selling_price   = $value->selling_price;
                            $prod->quantity        = $value->quantity;
                            $prod->description     = $value->description;
                            $prod->barcode         = $value->barcode;
                            $prod->save();
                        }
                        else {
                            $product                  = new Product();
                            $product->created_at      = $value->created_at;
                            $product->updated_at      = $value->updated_at;
                            $product->sync            = "on";
                            $product->name            = $value->name;
                            $product->categories      = $value->categories;
                            $product->brand_id        = $value->brand_id;
                            $product->cost_price      = $value->cost_price;
                            $product->selling_price   = $value->selling_price;
                            $product->quantity        = $value->quantity;
                            $product->description     = $value->description;
                            $product->barcode         = $value->barcode;
                            $product->save();
                        }
                    }
                    $is_conn = "Successfully sync product";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            case 'orders':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $data = "offline";
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/product/sendOrderToLocal', [$data]);
                    $decoded = json_decode($response);
                    foreach ($decoded as $value){
                        $ord = Order::where('order_number', $value->order_number)->first();
                        if (!empty($ord)){
                            $ord->created_at      = $value->created_at;
                            $ord->updated_at      = $value->updated_at;
                            $ord->sync            = "on";
                            $ord->order_number    = $value->order_number;
                            $ord->salesperson     = $value->salesperson;
                            $ord->customer_id     = $value->customer_id;
                            $ord->quantity        = $value->quantity;
                            $ord->amount_paid     = $value->amount_paid;
                            $ord->total_price     = $value->total_price;
                            $ord->payment_type    = $value->payment_type;
                            $ord->orderItems      = $value->orderItems;
                            $ord->save();
                        }
                        else {
                            $orders                  = new Order();
                            $orders->created_at      = $value->created_at;
                            $orders->updated_at      = $value->updated_at;
                            $orders->sync            = "on";
                            $orders->order_number    = $value->order_number;
                            $orders->salesperson     = $value->salesperson;
                            $orders->customer_id     = $value->customer_id;
                            $orders->quantity        = $value->quantity;
                            $orders->amount_paid     = $value->amount_paid;
                            $orders->total_price     = $value->total_price;
                            $orders->payment_type    = $value->payment_type;
                            $ord->orderItems         = $value->orderItems;
                            $ord->save();
                        }
                    }
                    $is_conn = "Successfully sync orders";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            case 'brand':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $data = "offline"; // echo "yes";  exit;
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/brand/sendBrandToLocal', [$data]);
                    $decoded = json_decode($response);
                    foreach ($decoded as $value){
                        $brand = Brand::where('brand_no', $value->brand_no)->first();
                        if (!empty($brand)){
                            $brand->created_at      = $value->created_at;
                            $brand->updated_at      = $value->updated_at;
                            $brand->sync            = "on";
                            $brand->name            = $value->name;
                            $brand->save();
                        }
                        else {
                            $br                  = new Brand();
                            $br->created_at      = $value->created_at;
                            $br->updated_at      = $value->updated_at;
                            $br->sync            = "on";
                            $br->name            = $value->name;
                            $br->brand_no        = $value->brand_no;
                            $br->save();
                        }
                    }
                    $is_conn = "Successfully sync brand";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            case 'category':
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $data = "offline";
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/category/sendCategoryToLocal', [$data]);
                    $decoded = json_decode($response);
                    foreach ($decoded as $value){
                        $category = Category::where('category_no', $value->category_no)->first();
                        if (!empty($category)){
                            $category->created_at      = $value->created_at;
                            $category->updated_at      = $value->updated_at;
                            $category->sync            = "on";
                            $category->name            = $value->name;
                            $category->category_no     = $value->category_no;
                            $category->save();
                        }
                        else {
                            $cat                  = new Category();
                            $cat->created_at      = $value->created_at;
                            $cat->updated_at      = $value->updated_at;
                            $cat->sync            = "on";
                            $cat->name            = $value->name;
                            $cat->category_no     = $value->category_no;
                            $cat->save();
                        }

                    }
                    $is_conn = "Successfully sync category";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            case 'customers' :
                $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
                if ($connected){
                    $data = "offline";
                    $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/sendCustomerToLocal', [$data]);
                    $decoded = json_decode($response);
                    foreach ($decoded as $value){
                        $customer = Customer::where('phone_no', $value->phone_no)->first();
                        if (!empty($customer)){
                            $customer->created_at      = $value->created_at;
                            $customer->updated_at      = $value->updated_at;
                            $customer->sync            = "on";
                            $customer->full_name       = $value->full_name;
                            $customer->email           = $value->email;
                            $customer->phone_no        = $value->phone_no;
                            $customer->address         = $value->address;
                            $customer->birthdate       = $value->birthdate;
                            $customer->registered_by   = $value->registered_by;
                            $customer->save();
                        }
                        else {
                            $cu                  = new Customer();
                            $cu->created_at      = $value->created_at;
                            $cu->updated_at      = $value->updated_at;
                            $cu->sync            = "on";
                            $cu->full_name       = $value->full_name;
                            $cu->email           = $value->email;
                            $cu->phone_no        = $value->phone_no;
                            $cu->address         = $value->address;
                            $cu->birthdate       = $value->birthdate;
                            $cu->registered_by   = $value->registered_by;
                            $cu->save();
                        }
                    }
                    $is_conn = "Successfully sync customers";
                    fclose($connected);
                }else{
                    $is_conn = "Connect to internet"; //action in connection failure
                }
                return $is_conn;
                break;
            default:
                return "Unknown error";
        }


    }

    protected static function get_order_number() {
        $last_order = DB::table('orders')->latest('created_at')->first();
        $system_num = "01";
        $order_numb = 0;
        $date = date("ymd");
        if (empty($last_order->order_number)) {
            $n = 1;
            $n = sprintf('%04u', $n);
            $order_numb = $date . $system_num . $n;
            return $order_numb;
        }
        else {
            $last_date = substr($last_order->order_number, 0, 6);
            if ($last_date != $date) {
                $n = 1;
                $n = sprintf('%04u', $n);
                return $order_numb = $date . $system_num . $n;
            } else {
                $last_order->order_number++;
                return $order_numb = $last_order->order_number;
            }
        }
    }

    public function getRecentSales() {
        $orders = Order::orderBy('created_at', 'DESC')
            ->where('returnStatus', 'NO')
            ->paginate(100);
        $index = 1;
        return view('orders.sales', ['orders' => $orders, 'index' => $index]);
    }

    public function getPrintPage($id) {
        $order = Order::find($id);
        $order->orderItems = unserialize($order->orderItems);
        return view('orders.print', ['order' => $order, 'carts' => $order->orderItems]);
    }

    public function getRePrintPage($id) {
        $order = Order::find($id);
        $order->orderItems = unserialize($order->orderItems);
        return view('orders.re-print', ['order' => $order, 'carts' => $order->orderItems]);
    }

    public function getMailReceipt($id){
        $order = Order::find($id);
        $order->orderItems = unserialize($order->orderItems);
      //  $customer = Customer::find($o)
        return view('orders.mail_receipt', ['order' => $order, 'carts' => $order->orderItems]);
    }

    public function postMailReceipt(Request $request, Mailer $mailer)
    {
        $receiver = $request['receiver_email'];
        $order_id = $request['order'];
        $order = Order::find($order_id);
        $carts = unserialize($order->orderItems);
        if ($mailer->to($receiver)->send(new ReceiptMail($order, $carts))){
            return view('orders.mail_receipt', ['order' => $order, 'carts' => $carts])
                ->with(['info' => 'Successful']);
        }
        return redirect()->back()->with('fail', 'Unable to send mail!');
      //  $mailer->to($receiver)->send(new ReceiptMail($order, $carts));
    }


    public function getReturnPage($id) {
        $order = Order::find($id);
        $order->orderItems = unserialize($order->orderItems);
        return view('orders.return', ['order' => $order, 'carts' => $order->orderItems]);
    }

    public function fetch(Request $request){
        if ($request->get('query')){
            $query = $request->get('query');
            //  echo $query; exit;
            $data  = DB::table('products')
                ->where('name', 'LIKE', "%{$query}%")
                ->get();
            //  print_r($data); exit;
            $output = '<ul class="dropdown-menu"
                style="display:block;
                position:relative">';
            foreach ($data as $row){
                $output .= '<li class="form-control drug"><a href="#">'.$row->name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    public function salesReport(){
        return View('reports.sales');
    }



    public function getSalesDetails(Request $request)
    {
        $index = 1;
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $orders = Order::whereBetween('created_at', [$startDate." 00:00:00", $endDate." 23:59:59"])
            ->where('returnStatus', 'NO')->get();

        $total_sum = Order::sum('total_price');
        $discounted_sum = Order::sum('amount_paid');

        return view('reports.salesDetails', ['orders' => $orders, 'index' => $index,
        'total_sum' => $total_sum, 'discounted_sum' => $discounted_sum]);
    }

    public function getReportBySalesPerson() {
        $users = User::all();
        return View('reports.salesPerson' , ['users' => $users]);
    }

    public function postReportBySalesPerson(Request $request) {
        $index = 1;
        $users = User::all();
        $salesPerson = $request['sales_person'];
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $orders = Order::whereBetween('created_at', [$startDate." 00:00:00", $endDate." 23:59:59"])
            ->where('returnStatus', 'NO')
            ->where('salesperson', $salesPerson) ->get();
        return view('reports.salesPersonDetails', ['orders' => $orders, 'index' => $index, 'users' => $users ]);

/*        if (!empty($orders)){
            return view('reports.salesPersonDetails',
                ['orders' => $orders, 'index' => $index ])->with(['info' =>
                'Reports between '. $startDate. ' and '. $endDate. 'for ' .$salesPerson.   '!']);
        }
        return view('reports.salesPersonDetails',
            ['orders' => $orders, 'index' => $index ])->with(['info' =>
            'No sales report between'. $startDate. ' and '. $endDate. 'for ' .$salesPerson.  '!']);*/

    }



    public function template(){
        $endpoint = "http://my.domain.com/test.php";
        $client = new \GuzzleHttp\Client();
        $id = 5;
        $value = "ABC";

        $response = $client->request('GET', $endpoint, ['query' => [
            'key1' => $id,
            'key2' => $value,
        ]]);

        $statusCode = $response->getStatusCode();
        $content = $response->getBody();
    }

    public function testing() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){


            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }


    public  function sendProductOnline() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $product = Product::where('sync', $sync)->get();
            //  $response = Http::post('http://localhost/bolablaque_on/public/api/api/v1/product/getProductData',  [$product] );
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/product/getProductData', [$product]);
            $decoded = json_decode($response);
            //  echo gettype($decoded[0]);  echo "<br/>";
            foreach ($decoded[0] as $value){
                //   echo $value->barcode . "<br/>";
                $prod = Product::where('barcode', $value->barcode)->first();
                $prod->sync = "on";
                $prod->save();
            }
            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    public  function receiveProduct() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/product/sendProductToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $prod = Product::where('barcode', $value->barcode)->first();
                if (!empty($prod)){
                    $prod->created_at      = $value->created_at;
                    $prod->updated_at      = $value->updated_at;
                    $prod->sync            = "on";
                    $prod->name            = $value->name;
                    $prod->categories      = $value->categories;
                    $prod->brand_id        = $value->brand_id;
                    $prod->cost_price      = $value->cost_price;
                    $prod->selling_price   = $value->selling_price;
                    $prod->quantity        = $value->quantity;
                    $prod->description     = $value->description;
                    $prod->barcode         = $value->barcode;
                    $prod->save();
                }
                else {
                    $product                  = new Product();
                    $product->created_at      = $value->created_at;
                    $product->updated_at      = $value->updated_at;
                    $product->sync            = "on";
                    $product->name            = $value->name;
                    $product->categories      = $value->categories;
                    $product->brand_id        = $value->brand_id;
                    $product->cost_price      = $value->cost_price;
                    $product->selling_price   = $value->selling_price;
                    $product->quantity        = $value->quantity;
                    $product->description     = $value->description;
                    $product->barcode         = $value->barcode;
                    $product->save();
                }
            }
            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;

    }


    public  function sendOrdersOnline() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $order = Order::where('sync', $sync)->get();
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/product/getOrderData', [$order]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                //   echo $value->barcode . "<br/>";
                $order = Order::where('order_number', $value->order_number)->first();
                $order->sync = "on";
                $order->save();
            }
            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;



    }

    public  function receiveOrders() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/product/sendOrderToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $ord = Order::where('order_number', $value->order_number)->first();
                if (!empty($ord)){
                    $ord->created_at      = $value->created_at;
                    $ord->updated_at      = $value->updated_at;
                    $ord->sync            = "on";
                    $ord->order_number    = $value->order_number;
                    $ord->salesperson     = $value->salesperson;
                    $ord->customer_id     = $value->customer_id;
                    $ord->quantity        = $value->quantity;
                    $ord->amount_paid     = $value->amount_paid;
                    $ord->total_price     = $value->total_price;
                    $ord->payment_type    = $value->payment_type;
                    $ord->orderItems      = $value->orderItems;
                    $ord->save();
                }
                else {
                    $orders                  = new Order();
                    $orders->created_at      = $value->created_at;
                    $orders->updated_at      = $value->updated_at;
                    $orders->sync            = "on";
                    $orders->order_number    = $value->order_number;
                    $orders->salesperson     = $value->salesperson;
                    $orders->customer_id     = $value->customer_id;
                    $orders->quantity        = $value->quantity;
                    $orders->amount_paid     = $value->amount_paid;
                    $orders->total_price     = $value->total_price;
                    $orders->payment_type    = $value->payment_type;
                    $ord->orderItems         = $value->orderItems;
                    $ord->save();
                }
            }

            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }



// Online Methods-----------------------------------------------------------
    public function getProductData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        //  dd(print_r($decoded));
        foreach ($decoded[0] as $item){
            // echo gettype($item);
            $product = Product::where('barcode', $item->barcode)->first();
            if (!empty($product)){
                $product->created_at      = $item->created_at;
                $product->updated_at      = $item->updated_at;
                $product->sync            = "on";
                $product->name            = $item->name;
                $product->categories      = $item->categories;
                $product->brand_id        = $item->brand_id;
                $product->cost_price      = $item->cost_price;
                $product->selling_price   = $item->selling_price;
                $product->quantity        = $item->quantity;
                $product->description     = $item->description;
                $product->barcode         = $item->barcode;
                $product->save();
            }
            else {
                $prod                  = new Product();
                $prod->created_at      = $item->created_at;
                $prod->updated_at      = $item->updated_at;
                $prod->sync            = "on";
                $prod->name            = $item->name;
                $prod->categories      = $item->categories;
                $prod->brand_id        = $item->brand_id;
                $prod->cost_price      = $item->cost_price;
                $prod->selling_price   = $item->selling_price;
                $prod->quantity        = $item->quantity;
                $prod->description     = $item->description;
                $prod->barcode         = $item->barcode;
                $prod->save();
            }
        }
        return $decoded;
    }

    public function sendProductToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $product = Product::where('sync', $sync)->get();
        foreach ($product as $p){
            $p->sync =  "on";
            $p->save();
        }
        return $product;
    }


    public function getOrderData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        //  dd(print_r($decoded));
        foreach ($decoded[0] as $item){
            // echo gettype($item);
            $ord = Order::where('order_number', $item->order_number)->first();
            if (!empty($ord)){
                $ord->created_at      = $item->created_at;
                $ord->updated_at      = $item->updated_at;
                $ord->sync            = "on";
                $ord->order_number    = $item->order_number;
                $ord->salesperson     = $item->salesperson;
                $ord->customer_id     = $item->customer_id;
                $ord->quantity        = $item->quantity;
                $ord->amount_paid     = $item->amount_paid;
                $ord->total_price     = $item->total_price;
                $ord->payment_type    = $item->payment_type;
                $ord->orderItems      = $item->orderItems;
                $ord->returnStatus    = $item->returnStatus;
                $ord->save();
            }
            else {
                $orders                  = new Order();
                $orders->created_at      = $item->created_at;
                $orders->updated_at      = $item->updated_at;
                $orders->sync            = "on";
                $orders->order_number    = $item->order_number;
                $orders->salesperson     = $item->salesperson;
                $orders->customer_id     = $item->customer_id;
                $orders->quantity        = $item->quantity;
                $orders->amount_paid     = $item->amount_paid;
                $orders->total_price     = $item->total_price;
                $orders->payment_type    = $item->payment_type;
                $orders->orderItems      = $item->orderItems;
                $orders->returnStatus    = $item->returnStatus;
                $orders->save();
            }
        }
        return $decoded;
    }

    public function sendOrderToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $order = Order::where('sync', $sync)->get();
        foreach ($order as $ord){
            $ord->sync =  "on";
            $ord->save();
        }
        return $order;
    }







}
