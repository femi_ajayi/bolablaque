<?php

namespace App\Http\Controllers;


use App\Customer;
use App\Order;
use App\ServiceResult;
use App\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;


class CustomerController extends Controller
{
    public function viewCustomerIndex()
    {
        //  $customers = Customer::all();
        $customers = Customer::orderBy('full_name')->paginate(20);
        $index = 1;
        return view('customer.index', ['customers' => $customers, 'index' => $index]);
    }

    public function viewCustomerByName(Request $request){
        $index = 1;
        $query  = $request['query'];
        $customers  = DB::table('customers')
            ->where('full_name', 'LIKE', "%{$query}%")
            ->get();
        return view('customer.customerName', ['customers' => $customers, 'index' => $index]);
    }

    public function getCustomerCreate()
    {
        return view('customer.create');
    }

    public function postCustomerCreate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone_no' => 'required|unique:customers',

        ]);

        $customer = new Customer();
        $customer->sync = "off";
        $customer->full_name = $request['name'];
        $customer->email     = $request['email'];
        $customer->phone_no  = $request['phone_no'];
        $customer->address   = $request['address'];
        if (empty($request['birthdate'])){
            $customer->birthdate = $request['birthdate'];
        } else {
            $customer->birthdate = date("Y-m-d", strtotime($request['birthdate']));
        }
        $customer->registered_by = Auth::user()->first_name . " " . Auth::user()->last_name;
        if ($customer->save()) {
            return redirect()->route('customer.index')->with(['info' => 'Customer Successfully registered']);
        }
        return redirect()->back()->with('fail', 'Unable to create customer!');
    }

    public function getCustomerEdit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit', ['customer' => $customer]);
    }

    public function ViewCustomerDetails($id) {
        $customer = Customer::find($id);
        $orders = Order::where('customer_id', $customer->id)->paginate(20);
        return view('customer.view', ['customer' => $customer, 'orders' => $orders]);
    }

    public function getOrderDetails($id) {
        $customer = Customer::find($id);
        $order = Order::where('customer_id', $customer->id)->first();
        $order->orderItems = unserialize($order->orderItems);
        return view('customer.orderDetails', ['order' => $order, 'carts' => $order->orderItems]);
    }

    public function getCustomerBirthdays() {
        $index = 1;
        $date = now();
        $customers = Customer::whereMonth('birthdate', '=', $date->month)
            ->whereDay('birthdate', '=', $date->day)
            ->get();
        return view('customer.birthdays', ['customers' => $customers, 'index' => $index]);
    }

    public function postCustomerUpdate(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'phone_no'      => 'required'
          //  'phone_no'      => 'required'
        ]);

        $customer = Customer::find($request->input('id'));
        $customer->sync          = "off";
        $customer->full_name     = $request->input('name');
        $customer->email         = $request->input('email');
        $customer->phone_no      = $request['phone_no'];
        $customer->address       = $request['address'];
        if (empty($request['birthdate'])){
            $customer->birthdate = $request['birthdate'];
        } else {
            $customer->birthdate = date("Y-m-d", strtotime($request['birthdate']));
        }
        if ($customer->save()){
            return redirect()->route('customer.index')->with('info', 'Customer Updated!' );
        }
        return redirect()->back()->with('fail', 'Could not update customer!');
    }

    public function getCustomerDelete($id)
    {
        $customer = Customer::find($id);
        return view('customer.delete', ['customer' => $customer]);
    }

    public function postCustomerDelete(Request $request)
    {
        $customer = Customer::find($request->input('id'));
        if ($customer->delete()){
            return redirect()->route('customer.index')->with(['success' => 'Successfully deleted']);
        }
        return redirect()->back()->with('fail', 'Could not delete customer!');
    }

    public function getCustomerDetails(Request $request)
    {
        $phone_no = $request['number'];
        $customer = Customer::where('phone_no', $phone_no)->first();

        Session::put('customer', $customer);
        Session::put('phone_number', $phone_no);

        $result = new ServiceResult();
        $result->message = "Successful";
        $result->success = true;
        $result->customer_details = UserCart::customer_details();

        return response()->json($result);

    }

    public function getCustomerNameDetails(Request $request)
    {
        $name = $request->get('name');
        $customers  = DB::table('customers')
            ->where('full_name', 'LIKE', "%{$name}%")
            ->get();

        Session::put('customers', $customers);
        $result = new ServiceResult();
        $result->message = "Successful";
        $result->success = true;
        $result->customer_details = UserCart::customer_name_details();
        return response()->json($result);
    }

    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('customers')
                ->where('full_name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '
           <li><a href="#">' . $row->full_name . '</a></li>
           ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

/*    public function searchCustomerByName(Request $request) {
        $index = 1;
        $search    = $request['customer_name'];
        $customers = DB::table('customers')
            ->where('full_name','like',"%".$search."%");
        return view('customer.index', ['customers' => compact($customers), 'index' => $index]);
      //  return view('customer.index',[compact('customers'), 'index' => $index ]);
    }*/

    public  function sendCustomerOnline() {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $customer = Customer::where('sync', $sync)->get();
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/getCustomerData', [$customer]);
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $cu = Customer::where('phone_no', $value->phone_no)->first();
                $cu->sync = "on";
                $cu->save();
            }

            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    public  function receiveCustomer() {
        $connected = @fsockopen("stores.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://stores.bolablaquebeauty.com/api/api/v1/customer/sendCustomerToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $customer = Customer::where('phone_no', $value->phone_no)->first();
                if (!empty($customer)){
                    $customer->created_at      = $value->created_at;
                    $customer->updated_at      = $value->updated_at;
                    $customer->sync            = "on";
                    $customer->full_name       = $value->full_name;
                    $customer->email           = $value->email;
                    $customer->phone_no        = $value->phone_no;
                    $customer->address         = $value->address;
                    $customer->birthdate       = $value->birthdate;
                    $customer->registered_by   = $value->registered_by;
                    $customer->save();
                }
                else {
                    $cu                  = new Customer();
                    $cu->created_at      = $value->created_at;
                    $cu->updated_at      = $value->updated_at;
                    $cu->sync            = "on";
                    $cu->full_name       = $value->full_name;
                    $cu->email           = $value->email;
                    $cu->phone_no        = $value->phone_no;
                    $cu->address         = $value->address;
                    $cu->birthdate       = $value->birthdate;
                    $cu->registered_by   = $value->registered_by;
                    $cu->save();
                }
            }

            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;

    }

// Online Methods-----------------------------------------------------------
    public function getCustomerData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        foreach ($decoded[0] as $item){
            $customer = Customer::where('phone_no', $item->phone_no)->first();
            if (!empty($customer)){
                $customer->created_at      = $item->created_at;
                $customer->updated_at      = $item->updated_at;
                $customer->sync            = "on";
                $customer->full_name       = $item->full_name;
                $customer->email           = $item->email;
                $customer->phone_no        = $item->phone_no;
                $customer->address         = $item->address;
                $customer->birthdate       = $item->birthdate;
                $customer->registered_by   = $item->registered_by;
                $customer->save();
            }
            else {
                $cu                  = new Customer();
                $cu->created_at      = $item->created_at;
                $cu->updated_at      = $item->updated_at;
                $cu->sync            = "on";
                $cu->full_name       = $item->full_name;
                $cu->email           = $item->email;
                $cu->phone_no        = $item->phone_no;
                $cu->address         = $item->address;
                $cu->birthdate       = $item->birthdate;
                $cu->registered_by   = $item->registered_by;
                $cu->save();
            }
        }
        return $decoded;
    }

    public function sendCustomerToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $customer = Customer::where('sync', $sync)->get();
        foreach ($customer as $c){
            $c->sync =  "on";
            $c->save();
        }
        return $customer;
    }








}
