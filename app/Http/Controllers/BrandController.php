<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class BrandController extends Controller
{
    public function viewBrandIndex()
    {
        //  $brands = Brand::all();
        $brands = Brand::orderBy('name')->paginate(20);
        $index = 1;
        return view('brand.index', ['brands' => $brands, 'index' => $index]);
    }

    public function viewBrandByName(Request $request){
        $index = 1;
        $query  = $request['query'];
        $brands  = DB::table('brands')
            ->where('name', 'LIKE', "%{$query}%")
            ->get();
        return view('brand.brandName', ['brands' => $brands, 'index' => $index]);
    }


    public function getBrandCreate()
    {
        return view('brand.create');
    }

    public function postBrandCreate(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|unique:brands',
            'brand_no'  => 'unique:brands'
        ]);

        $brand              = new Brand();
        $brand->sync        = "off";
        $brand->name        = $request['name'];
        $brand->brand_no    = rand(100000,999999);
        if ($brand->save()){
            return redirect()->route('brand.index')->with(['info' => 'Brand Successfully created']);
        }
    }


    public function getBrandEdit($id)
    {
        $brand = Brand::find($id);
        return view('brand.edit', ['brand' => $brand]);
    }


    public function postBrandUpdate(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required',
        ]);

        $brand                  = Brand::find($request->input('id'));
        $brand->sync            = "off";
        $brand->name            = $request->input('name');
        if ($brand->save()){
            return redirect()->route('brand.index')->with('info', 'Brand Updated!' );
        }
        return redirect()->back()->with('fail', 'Could not update brand!');

    }

    public function getBrandDelete($id)
    {
        $brand = Brand::find($id);
        return view('brand.delete', ['brand' => $brand]);
    }

    public function postBrandDelete(Request $request)
    {
        $brand = Brand::find($request->input('id'));
        if ($brand->delete()){
            return redirect()->route('brand.index')->with(['success' => 'Successfully deleted']);
        }
        return redirect()->back()->with('fail', 'Could not delete brand!');
    }

    public function addBrandNumber(){
        $brands = Brand::where('brand_no', 0)->get();
        foreach ($brands as $brand) {
            $qrcontent = rand(100000, 999999);
            $brand->brand_no = $qrcontent;
            $brand->save();
        }
    }





    public  function sendBrandOnline() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $sync = "off";
            $brand = Brand::where('sync', $sync)->get(); //  echo $brand; dd("lkk");
            //   $response = Http::post('http://localhost/bolablaque_on/public/api/api/v1/brand/getBrandData',  [$brand] );
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/brand/getBrandData', [$brand]);
            //  echo $response;  dd("pi");
            $decoded = json_decode($response);
            foreach ($decoded[0] as $value){
                $br = Brand::where('brand_no', $value->brand_no)->first();
                $br->sync = "on";
                $br->save();
            }
            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    public  function receiveBrand() {
        $connected = @fsockopen("store.bolablaquebeauty.com", 80);
        if ($connected){
            $data = "offline";
            $response = Http::post('http://store.bolablaquebeauty.com/api/api/v1/brand/sendBrandToLocal', [$data]);
            $decoded = json_decode($response);
            foreach ($decoded as $value){
                $brand = Brand::where('brand_no', $value->brand_no)->first();
                if (!empty($brand)){
                    $brand->created_at      = $value->created_at;
                    $brand->updated_at      = $value->updated_at;
                    $brand->sync            = "on";
                    $brand->name            = $value->name;
                    $brand->save();
                }
                else {
                    $br                  = new Brand();
                    $br->created_at      = $value->created_at;
                    $br->updated_at      = $value->updated_at;
                    $br->sync            = "on";
                    $br->name            = $value->name;
                    $br->brand_no        = $value->brand_no;
                    $br->save();
                }
            }

            $is_conn = true;
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;


    }

// Online Methods-----------------------------------------------------------
    public function getBrandData(Request $request)
    {
        $decoded = json_decode($request->getContent());
        foreach ($decoded[0] as $item){
            $brand = Brand::where('brand_no', $item->brand_no)->first();
            if (!empty($brand)){
                $brand->created_at      = $item->created_at;
                $brand->updated_at      = $item->updated_at;
                $brand->sync            = "on";
                $brand->name            = $item->name;
                $brand->save();
            }
            else {
                $br                  = new Brand();
                $br->created_at      = $item->created_at;
                $br->updated_at      = $item->updated_at;
                $br->sync            = "on";
                $br->name            = $item->name;
                $br->brand_no        = $item->brand_no;
                $br->save();
            }
        }
        return $decoded;
    }

    public function sendBrandToLocal(Request $request){
        $decoded = json_decode($request->getContent());
        $sync = "off";
        $brand = Brand::where('sync', $sync)->get();
        foreach ($brand as $b){
            $b->sync =  "on";
            $b->save();
        }
        return $brand;
    }









}
